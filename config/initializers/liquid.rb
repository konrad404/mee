# frozen_string_literal: true

require "liquid/stage_in"
require "liquid/stage_in_by_filename"
require "liquid/stage_out"
require "liquid/clone_repo"
require "liquid/license_for"
require "liquid/value_of"
require "liquid/stage_in_artifact"
require "liquid/stage_out_artifact"
require "liquid/dataverse_dataset_stage_in"
require "liquid/dataverse_file_stage_out"
require "liquid/dataverse_file_stage_in"

Liquid::Template.register_tag("stage_in", Liquid::StageIn)
Liquid::Template.register_tag("stage_in_by_filename", Liquid::StageInByFilename)
Liquid::Template.register_tag("stage_out", Liquid::StageOut)
Liquid::Template.register_tag("clone_repo", Liquid::CloneRepo)
Liquid::Template.register_tag("license_for", Liquid::LicenseFor)
Liquid::Template.register_tag("value_of", Liquid::ValueOf)
Liquid::Template.register_tag("stage_in_artifact", Liquid::StageInArtifact)
Liquid::Template.register_tag("stage_out_artifact", Liquid::StageOutArtifact)
Liquid::Template.register_tag("dataverse_dataset_stage_in", Liquid::DataverseDatasetStageIn)
Liquid::Template.register_tag("dataverse_file_stage_out", Liquid::DataverseFileStageOut)
Liquid::Template.register_tag("dataverse_file_stage_in", Liquid::DataverseFileStageIn)
