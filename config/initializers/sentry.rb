# frozen_string_literal: true

if Rails.env.production? && ENV["SENTRY_DSN"]
  Sentry.init do |config|
    config.environment = ENV["SENTRY_ENVIRONMENT"] || "production"
    config.dsn = ENV["SENTRY_DSN"]
    config.breadcrumbs_logger = [:active_support_logger, :http_logger]
    config.traces_sample_rate = 0.5
    config.send_default_pii = true

    config.transport.ssl_verification = ENV["SENTRY_DISABLE_SSL_VERIFICATION"] != "true"
  end
end
