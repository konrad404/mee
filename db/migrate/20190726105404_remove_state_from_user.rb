# frozen_string_literal: true

class RemoveStateFromUser < ActiveRecord::Migration[5.2]
  def change
    remove_column :users, :state, :integer, default: 0, null: false
  end
end
