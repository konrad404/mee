# frozen_string_literal: true

class CreatePersistentErrorAndAddToOrganization < ActiveRecord::Migration[6.0]
  def change
    create_table :persistent_errors do |t|
      t.string :key, index: true
      t.string :message, null: false
    end
    add_reference :persistent_errors, :errorable, polymorphic: true, null: false
    add_column :organizations, :persistent_errors_count, :integer
  end
end
