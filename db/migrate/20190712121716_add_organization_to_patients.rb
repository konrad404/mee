# frozen_string_literal: true

class AddOrganizationToPatients < ActiveRecord::Migration[5.2]
  def change
    add_reference :patients, :organization, foreign_key: true, index: true
  end
end
