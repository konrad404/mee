# frozen_string_literal: true

class AddDiscardedAtToSteps < ActiveRecord::Migration[6.1]
  def change
    add_column :steps, :discarded_at, :timestamp
    add_index :steps, :discarded_at
  end
end
