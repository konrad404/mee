# frozen_string_literal: true

class RemoveDataTypeEnumFromDataFile < ActiveRecord::Migration[5.2]
  def change
    remove_column :data_files, :data_type_enum, :integer
  end
end
