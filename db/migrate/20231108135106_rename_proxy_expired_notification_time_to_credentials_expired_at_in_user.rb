# frozen_string_literal: true

class RenameProxyExpiredNotificationTimeToCredentialsExpiredAtInUser < ActiveRecord::Migration[7.0]
  def change
    rename_column :users, :proxy_expired_notification_time, :credentials_expired_at
  end
end
