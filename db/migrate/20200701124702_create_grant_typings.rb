# frozen_string_literal: true

class CreateGrantTypings < ActiveRecord::Migration[6.0]
  def change
    create_table :grant_typings do |t|
      t.belongs_to :grant, index: true, null: false
      t.belongs_to :grant_type, index: true, null: false
      t.timestamps
    end

    add_index :grant_typings, [:grant_id, :grant_type_id], unique: true
  end
end
