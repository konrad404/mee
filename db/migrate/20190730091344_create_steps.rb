# frozen_string_literal: true

class CreateSteps < ActiveRecord::Migration[5.2]
  def change
    create_table :steps do |t|
      t.string :name, null: false
      t.string :slug, null: false
      t.jsonb :config, null: false
      t.string :type, null: false
      t.belongs_to :organization

      t.timestamps
    end
    add_index :steps, :slug, unique: true
  end
end
