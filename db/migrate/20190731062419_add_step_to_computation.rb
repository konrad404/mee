# frozen_string_literal: true

class AddStepToComputation < ActiveRecord::Migration[5.2]
  def change
    add_reference :computations, :step, foreign_key: true
  end
end
