# frozen_string_literal: true

class ChangeSitePrefixesToHostKey < ActiveRecord::Migration[7.0]
  def up
    add_column :sites, :host_key, :string

    ActiveRecord::Base.connection.execute(
      <<~SQL
        UPDATE sites SET
          host_key = RIGHT(LEFT(download_url_prefix, LENGTH(download_url_prefix) - 1), LENGTH(download_url_prefix) - 11)
      SQL
    )
    change_column_null :sites, :host_key, false

    remove_column :sites, :download_url_prefix
    remove_column :sites, :upload_url_prefix
  end

  def down
    add_column :sites, :download_url_prefix, :string
    add_column :sites, :upload_url_prefix, :string

    ActiveRecord::Base.connection.execute(
      <<~SQL
        UPDATE sites SET
          download_url_prefix = '/download/' || host_key || '/',
          upload_url_prefix = '/upload/' || host_key || '/'
      SQL
    )

    change_column_null :sites, :download_url_prefix, false
    change_column_null :sites, :upload_url_prefix, false

    remove_column :sites, :host_key
  end
end
