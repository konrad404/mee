# frozen_string_literal: true

class AddStorageConfigToOrganization < ActiveRecord::Migration[7.0]
  def up
    add_column :organizations, :storage_config, :jsonb

    ActiveRecord::Base.connection.execute(
      <<~SQL
        UPDATE organizations SET
          storage_config = jsonb_set(
            '{"type": "plgrid", "host": "prometheus.cyfronet.pl", "host_key": "prometheus"}',
            '{path}', to_jsonb(path))
      SQL
    )

    change_column_null :organizations, :storage_config, false
    remove_column :organizations, :path, :string
  end

  def down
    add_column :organizations, :path, :string
    ActiveRecord::Base.connection.execute(
      <<~SQL
        UPDATE organizations SET path = storage_config ->> 'path'
      SQL
    )

    change_column_null :organizations, :path, false
    remove_column :organizations, :storage_config, :jsonb
  end
end
