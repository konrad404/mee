# frozen_string_literal: true

class AddMigratedToOrganization < ActiveRecord::Migration[7.0]
  def change
    add_column :organizations, :migrated, :boolean, default: false
  end
end
