# frozen_string_literal: true

class AddCheckinAutoacceptUrnToOrganization < ActiveRecord::Migration[6.0]
  def change
    add_column :organizations, :checkin_autoaccept_urn, :string
  end
end
