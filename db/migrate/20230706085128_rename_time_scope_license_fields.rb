# frozen_string_literal: true

class RenameTimeScopeLicenseFields < ActiveRecord::Migration[7.0]
  def change
    rename_column :licenses, :start_at, :starts_at
    rename_column :licenses, :end_at, :ends_at
  end
end
