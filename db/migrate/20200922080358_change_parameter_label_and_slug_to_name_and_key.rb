# frozen_string_literal: true

class ChangeParameterLabelAndSlugToNameAndKey < ActiveRecord::Migration[6.0]
  def change
    rename_column :parameters, :label, :name
    rename_column :parameters, :slug, :key
  end
end
