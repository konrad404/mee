# frozen_string_literal: true

class AddRepoConfigToRimrockStep < ActiveRecord::Migration[6.1]
  def change
    add_column :steps, :repo_config, :jsonb, default: {}, null: false
  end
end
