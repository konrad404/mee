# frozen_string_literal: true

class AddRolesToOrganization < ActiveRecord::Migration[5.2]
  def change
    add_column :organizations, :roles_mask, :integer
  end
end
