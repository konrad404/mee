# frozen_string_literal: true

class AddStorageQuotaToOrganization < ActiveRecord::Migration[7.0]
  def change
    add_column :organizations, :storage_quota, :integer, default: 5, null: false
  end
end
