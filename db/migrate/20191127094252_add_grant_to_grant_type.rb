# frozen_string_literal: true

class AddGrantToGrantType < ActiveRecord::Migration[5.2]
  def change
    add_reference :grants, :grant_type, index: true, foreign_key: true, null: false
  end
end
