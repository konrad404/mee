# frozen_string_literal: true

class AddDataverseIntegration < ActiveRecord::Migration[7.0]
  def change
    add_column :organizations, :dataverse_url, :string
    add_column :memberships, :dataverse_token, :string
  end
end
