# frozen_string_literal: true

class RemoveDomainFromOrganization < ActiveRecord::Migration[7.0]
  def change
    remove_column :organizations, :domain, :string
  end
end
