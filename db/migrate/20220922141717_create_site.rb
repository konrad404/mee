# frozen_string_literal: true

class CreateSite < ActiveRecord::Migration[7.0]
  def change
    create_table :sites do |t|
      t.string :name, null: false, unique: true
      t.string :url, null: false
      t.string :download_url_prefix, null: false
      t.string :upload_url_prefix, null: false
      t.timestamps
    end
  end
end
