# frozen_string_literal: true

class ChangeComputationStatusToInteger < ActiveRecord::Migration[7.0]
  def up
    rename_column :computations, :status, :status_old
    add_column :computations, :status,  :integer, default: 0

    ActiveRecord::Base.connection.execute(
      <<~SQL
        UPDATE computations
        SET status = CASE
        WHEN status_old = 'created' THEN 0
        WHEN status_old = 'new' THEN 1
        WHEN status_old = 'queued' THEN 2
        WHEN status_old = 'running' THEN 3
        WHEN status_old = 'finished' THEN 4
        WHEN status_old = 'aborted' THEN 5
        WHEN status_old = 'error' THEN 6
        ELSE 0
        END;
      SQL
    )

    remove_column :computations, :status_old
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
