# frozen_string_literal: true

class AddOrganizationToGroup < ActiveRecord::Migration[5.2]
  def change
    add_reference :groups, :organization, foreign_key: true, index: true
  end
end
