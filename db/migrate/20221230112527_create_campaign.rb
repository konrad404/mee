# frozen_string_literal: true

class CreateCampaign < ActiveRecord::Migration[7.0]
  def change
    create_table :campaigns do |t|
      t.references :cohort, foreign_key: true
      t.string :name, null: false
      t.references :organization, null: false, foreign_key: true
      t.integer :failed_pipelines, null: false, default: 0
      t.integer :desired_pipelines, null: false

      t.integer :status, default: 0

      t.timestamps
    end

    add_reference :pipelines, :campaign, foreign_key: true

    add_column :cohorts, :campaigns_count, :integer, default: 0, null: false
  end
end
