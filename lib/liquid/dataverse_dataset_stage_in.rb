# frozen_string_literal: true

module Liquid
  class DataverseDatasetStageIn < Liquid::Tag
    def initialize(tag_name, parameters, tokens)
      super
      @doi, @filename = parameters.split
    end

    def render(context)
      dataverse_entry = Liquid::DataverseEntry.new(context, @doi)
      url = dataverse_entry.dataset_url

      file = @filename.blank? ? "$SCRATCHDIR/$(basename #{url})" : "$SCRATCHDIR/#{@filename}"

      <<~COMMAND
        RESPONSE=$(curl -L -w "%{http_code}" -o #{file} -H "X-Dataverse-key:#{dataverse_entry.token}" #{url})
        HTTP_CODE=${RESPONSE: -3}
        if [ "${HTTP_CODE}" != "200" ]
        then
          >&2 echo "Failed to download Dataverse dataset with DOI: #{@doi}"
          >&2 cat #{file}
          rm #{file}
          exit 1
        elif [ -z "#{@filename}" ]
        then
          unzip #{file} -d $SCRATCHDIR
          rm #{file} $SCRATCHDIR/MANIFEST.TXT
        fi
      COMMAND
    end
  end
end
