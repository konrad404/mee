# frozen_string_literal: true

module Liquid
  class CloneRepo < Liquid::Tag
    def render(context)
      clone_url = "git@#{context.registers[:computation].step.host}"
      ssh_download_key = context.registers[:computation].step.download_key
      repository = context.registers[:computation].step.repository

      <<~CODE
        export SSH_DOWNLOAD_KEY="#{ssh_download_key}"
        ssh-agent bash -c '
          ssh-add <(echo "$SSH_DOWNLOAD_KEY");
          git clone #{clone_url}:#{repository}
          cd `basename #{repository} .git`
          git reset --hard #{context['revision']}'
      CODE
    end
  end
end
