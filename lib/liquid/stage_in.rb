# frozen_string_literal: true

require "link_helper"

module Liquid
  class StageIn < Liquid::Tag
    include ::LinkHelper

    def initialize(tag_name, parameters, tokens)
      super
      parameters = parameters.split
      @data_file_type_string = parameters.shift
      @filename_to_save = parameters.join(" ") unless parameters.empty?
    end

    def render(context)
      computation = context.registers[:computation]
      filename = @filename_to_save || computation.pick_file_by_type(@data_file_type_string)&.filename
      if filename && @data_file_type_string
        url = computation_type_inputs_url(computation, secret: computation.secret,
          type: @data_file_type_string, script_name: script_name(computation))

        "curl --retry 5 --retry-max-time 600 --retry-connrefused -L -o \"$SCRATCHDIR/#{filename}\" #{url}"
      else
        context.registers[:errors]
          .add(:script, "cannot find #{@data_file_type_string} data file in patient or pipeline directories")
      end
    end
  end
end
