# frozen_string_literal: true

module Liquid
  class DataverseFileStageOut < Liquid::Tag
    def initialize(tag_name, parameters, tokens)
      super
      @relative_path, @doi, @metadata = parameters.split(" ", 3)
    end

    def render(context)
      dataverse_entry = Liquid::DataverseEntry.new(context, @doi)
      url = dataverse_entry.add_datafile_url

      response = "$SCRATCHDIR/$(basename #{url})"
      unless @metadata.blank?
        json_data = "-F jsonData='" + @metadata.to_s + "' "
      end

      <<~COMMAND
        RESPONSE=$(curl -w "%{http_code}" -o #{response} -H "X-Dataverse-key:#{dataverse_entry.token}" -X POST -F file=@#{@relative_path} #{json_data}#{url})
        HTTP_CODE=${RESPONSE: -3}
        if [ "${HTTP_CODE}" != "200" ]
        then
          >&2 echo "Failed to upload file: #{@relative_path}: "
          >&2 cat #{response}
          rm #{response}
          exit 1
        fi
        rm -f #{response}
      COMMAND
    end
  end
end
