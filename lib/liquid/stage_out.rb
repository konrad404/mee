# frozen_string_literal: true

require "link_helper"

module Liquid
  class StageOut < Liquid::Tag
    include ::LinkHelper

    def initialize(tag_name, relative_path, tokens)
      super
      @relative_path = relative_path.strip
      @relative_path.prepend("./") unless @relative_path.start_with?("./")
    end

    def render(context)
      computation = context.registers[:computation]
      url = computation_outputs_url(computation, secret: computation.secret,
        script_name: script_name(computation))
      # Imitate cp functionality eg. cp *.txt
      <<~COMMAND
        find . -type f -path "#{@relative_path}" | while read file; do
          response=$(curl -s -w "%{http_code}" -F "file=@$file" #{url})
          if [ $response -ne 201 ]; then
            echo "Failed to upload file $file, error code is $response" >&2
          fi
        done
      COMMAND
    end
  end
end
