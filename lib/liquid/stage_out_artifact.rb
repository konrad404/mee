# frozen_string_literal: true

require "link_helper"

module Liquid
  class StageOutArtifact < Liquid::Tag
    include ::LinkHelper

    def initialize(tag_name, filename, tokens)
      super
      @filename = filename.strip
    end

    def render(context)
      computation = context.registers[:computation]
      url = computation_upload_artifact_url(computation, secret: computation.secret,
        script_name: script_name(computation))

      <<~COMMAND
        response=$(curl -s -w "%{http_code}" -F "file=@#{@filename}" #{url})
        if [ $response -ne 201 ]; then
          echo "Failed to upload file $file, error code is $response" >&2
        fi
      COMMAND
    end
  end
end
