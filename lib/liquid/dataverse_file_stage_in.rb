# frozen_string_literal: true

module Liquid
  class DataverseFileStageIn < Liquid::Tag
    def initialize(tag_name, parameters, tokens)
      super
      @doi, @filename = parameters.split
    end

    def render(context)
      dataverse_entry = Liquid::DataverseEntry.new(context, @doi)
      url = dataverse_entry.datafile_url

      file = @filename.blank? ? "$(basename #{url})" : @filename
      output_option = @filename.blank? ? "-O" : "-o #{file}"

      <<~COMMAND
        LOCATION="$(pwd)"
        cd $SCRATCHDIR
        RESPONSE=$(curl -L #{output_option} -J -w "%{http_code}" -H "X-Dataverse-key:#{dataverse_entry.token}" #{url})
        HTTP_CODE=${RESPONSE: -3}
        if [ "${HTTP_CODE}" != "200" ]
        then
          >&2 echo "Failed to download Dataverse file with DOI: #{@doi}"
          >&2 cat #{file}
          rm #{file}
          exit 1
        fi
        cd $LOCATION
      COMMAND
    end
  end
end
