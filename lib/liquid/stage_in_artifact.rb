# frozen_string_literal: true

require "link_helper"

module Liquid
  class StageInArtifact < Liquid::Tag
    include ::LinkHelper

    def initialize(tag_name, filename, tokens)
      super
      @filename = filename.strip
    end

    def render(context)
      computation = context.registers[:computation]
      # Adding random name and then replacing it with actual filename
      # So that we can skip escaping filename if it is a bash variable
      url = computation_artifacts_url(computation, secret: computation.secret, name: "t", script_name: script_name(computation))
      url = url[0...-1].concat(@filename)
      "curl -L -o $SCRATCHDIR/#{@filename} #{url}"
    end
  end
end
