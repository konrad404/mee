# frozen_string_literal: true

require "link_helper"

module Liquid
  class StageInByFilename < Liquid::Tag
    include ::LinkHelper

    def initialize(tag_name, parameters, tokens)
      super
      parameters = parameters.split
      @filename = parameters.shift
      @filename_to_save = parameters.empty? ? @filename : parameters.join(" ")
    end

    def render(context)
      computation = context.registers[:computation]
      # Adding random name and then replacing it with actual filename
      # So that we can skip escaping filename if it is a bash variable
      url = computation_filename_inputs_url(computation, secret: computation.secret,
        filename: "t", script_name: script_name(computation))
      url = url[0...-1].concat(@filename)

      "curl --retry 5 --retry-max-time 600 --retry-connrefused -L -o \"$SCRATCHDIR/#{@filename_to_save}\" #{url}"
    end
  end
end
