# frozen_string_literal: true

require_relative "../config/boot"
require_relative "../config/environment"

module Clockwork
  every(Mee::Application.config.clock.update, "updating.computations") do
    TriggerUpdateJob.perform_later
  end

  if Rails.env.production?
    every(24.hours, "validate.organizations") do
      OrganizationsJob.perform_later
    end

    every(24.hours, "validate.flows") do
      FlowsJob.perform_later
    end

    every(24.hours, "validate.grants") do
      GrantsValidationsJob.perform_later
    end
  end
end
