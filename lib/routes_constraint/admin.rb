# frozen_string_literal: true

module RoutesConstraint
  class Admin
    def self.matches?(request)
      cookies = ActionDispatch::Cookies::CookieJar.build(request, request.cookies)
      user_id = cookies.signed["user.id"]

      User.find_by(id: user_id)&.admin? if user_id.present?
    end
  end
end
