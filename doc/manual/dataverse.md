
The Model Execution Environment facilitates seamless data access on Dataverse instances through its API.<br>
Users have the capability to effortlessly download particular files or entire datasets from Dataverse storage.<br>
Additionally, the environment enables users to upload data generated during simulations.<br>
It's crucial to ensure the accuracy of your Dataverse configuration, since pipelines utilizing the Liquid tags described below may become inoperable.

Documentation of the Dataverse can be found on the project site: <a href="https://guides.dataverse.org/en/latest/" class="btn-link">Dataverse Guide</a>

---

#### Configuration

---

In order to use Dataverse integration features your organization must have Dataverse instance address configured.<br>
To access data on Dataverse you need to provide API token in your user profile.<br>
The Dataverse token is configured per organization! That means if you belong to multiple organizations you want to use dataverse in, you have to set up a token in your profile in each of them.

---

#### Dataverse liquid tags

---

**{% dataverse_file_stage_out filePath persistentID metadata %}**

This tag is used to upload a file located at `filePath` to an existing dataset with the specific `persistentID` at the configured Dataverse instance.

- `filePath`: The path to the file that you want to upload to Dataverse. If file path includes whitespaces, this parameter must be passed as a predefined variable.
- `persistentID`: The identifier for the target dataset in Dataverse. It supports only DOIs (Digital Object Identifier) and should be provided in a specific format: "doi:<actual_doi>".<br>For example, "doi:10.12326/shoulder/23GF4H". Note that DOIs contain only uppercase letters except for the "shoulder" part.
- `metadata`: An optional parameter that can be included in JSON format to provide additional file metadata. <b>(OPTIONAL)</b> <br>For example:

```json
  {"directoryLabel":"dir1/subdir","categories":["Data"], "restrict":"false"}
```

The JSON attributes and their descriptions can be found at the Dataverse guide:  <a href="https://guides.dataverse.org/en/latest/api/native-api.html#add-file-api" class="btn-link">Dataverse Add File API</a>

**{% dataverse_file_stage_in persistentID target %}**

This tag is used to download a file with the specified `persistentID` from the Dataverse instance.

- `persistentID`: The identifier for the target file in Dataverse, provided in the DOI format (e.g., "doi:10.12326/shoulder/23GF4H").
- `target`: The name of the file where downloaded content should be saved. <b>(OPTIONAL)</b>

**{% dataverse_dataset_stage_in persistentID target %}**

This tag is used to download the content of a dataset, preserving the directory-tree structure, with the specified `persistentID` from the Dataverse instance.<br>By default, the dataset will be extracted to the $SCRATCHDIR - if `target` file is not provided.

- `persistentID`: The identifier for the target dataset in Dataverse, provided in the DOI format (e.g., "doi:10.12326/shoulder/23GF4H").
- `target`: The name of the zip file where the downloaded content should be saved. <b>(OPTIONAL)</b>


---
***NOTE***

*Please make sure to follow the specified formats for DOI and metadata (if used) to ensure correct functionality of the liquid tags.*<br>
*Please ensure that you are using quoted file/target paths when they contain spaces.*

