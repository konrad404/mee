# frozen_string_literal: true

source "https://rubygems.org"
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby "3.2.2"

gem "rails", "~> 7.0.0"
gem "pg", "~> 1.1"
gem "puma", "~> 5.0"

gem "sprockets-rails"
gem "cssbundling-rails"
gem "jsbundling-rails"
gem "stimulus-rails"


# Reduces boot times through caching; required in config/boot.rb
gem "bootsnap", require: false

# javascript
gem "gravtastic"
gem "haml-rails", "~> 2.0"
gem "turbo-rails"
gem "view_component"

gem "active_link_to"
gem "friendly_id"
gem "simple_form"
gem "image_processing", "~> 1.2"

gem "faraday"

# pagination
gem "pagy"

# app security
gem "rack-attack"

# Markdown
gem "github-markup"
gem "redcarpet"

gem "liquid"

gem "omniauth-openid"
gem "omniauth-rails_csrf_protection"
gem "ruby-openid", github: "mkasztelnik/ruby-openid", branch: "fix/openid-login-fail"
gem "pundit"
gem "role_model"

# Files store client
gem "net_dav"
gem "aws-sdk-s3", "~> 1.117"

# Delayed jobs
gem "clockwork"
gem "sidekiq", "< 7"

# Cache store
gem "redis-rails"

# File processing
gem "rubyzip", ">= 1.0.0"

# URL validation
gem "addressable", "~> 2.5"

# Blob validator
gem "activestorage-validator"

# Gitlab integration
gem "gitlab"

gem "redis", "~> 4.0"

gem "rack-proxy"

gem "ssh_data", github: "github/ssh_data"
gem "ed25519"

gem "rexml"

# soft delete
gem "discard", "~> 1.2"

group :development, :test do
  gem "bullet"
  gem "dotenv-rails"
  gem "i18n-tasks", "~> 1.0.4"
  gem "debug"
end

group :development do
  gem "listen", "~> 3.3"
  gem "spring"
  gem "web-console"
  gem "rack-mini-profiler"

  gem "rubocop-rails_config", require: false
end

group :test do
  gem "capybara", ">= 3.26"
  gem "launchy"
  gem "sshkey"
  gem "selenium-webdriver"
  gem "webmock", "~> 3.10"
  gem "fixture_factory"
  gem "mocha"
end

group :production do
  gem "newrelic_rpm"
  gem "sentry-ruby"
  gem "sentry-rails"
  gem "sentry-sidekiq"
end
