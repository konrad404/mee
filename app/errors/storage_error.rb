# frozen_string_literal: true

class StorageError < NameError; end
