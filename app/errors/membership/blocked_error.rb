# frozen_string_literal: true

class Membership::BlockedError < NameError; end
