# frozen_string_literal: true

class Membership::NotFoundError < NameError; end
