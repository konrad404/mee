# frozen_string_literal: true

module Pipelines
  class StartRunnable
    def initialize(pipeline)
      @pipeline = pipeline
      @user = pipeline.user
    end

    def call
      internal_call if @pipeline.automatic?
    end

    private
      def internal_call
        @pipeline.computations.created.each { |c| c.run if runnable?(c) }
      end

      def runnable?(computation)
        computation.runnable? &&
          @user.credentials_valid? &&
          computation.configured? &&
          !computation.organization.quota.exceeded?
      end
  end
end
