# frozen_string_literal: true

require "liquid"

class ScriptGenerator
  attr_reader :computation

  delegate :pipeline, :revision, to: :computation
  delegate :runnable, :user, :mode, to: :pipeline
  delegate :email, to: :user
  delegate :slug, :organization, to: :runnable

  def initialize(computation, template, errors = ActiveModel::Errors.new(computation))
    @computation = computation
    @template = template
    @errors = errors
  end

  def call
    if @template
      parsed_template = Liquid::Template.parse(@template)

      parsed_template
        .render({ "email" => email, "case_number" => (runnable.try(:case_number)),
                  "revision" => revision, "grant_id" => grant_id, "mode" => mode,
                  "pipeline_identifier" => pipeline_identifier, "proxy" => user.proxy.to_s,
                  "campaign_name" => pipeline.campaign&.name, "cohort_name" => pipeline.campaign&.cohort&.name,
                  "step_name" => @computation.name, "pipeline_name" => pipeline.name },
                registers: { pipeline:, computation:, organization:, errors: @errors })
        .gsub(/\r\n?/, "\n")
    end
  rescue Liquid::SyntaxError => e
    @errors.add(:script, e.message)
  end

  def grant_id
    grant = computation.parameter_value_for(Step::Parameters::GRANT)&.value

    grant || @errors.add(:script, "active grant cannot be found")
  end

  def pipeline_identifier
    "#{slug}-#{pipeline.iid}"
  end
end
