# frozen_string_literal: true

module Rimrock
  class Runner < Rimrock::Service
    EXIT_CODE_SUCCESS = 0

    def initialize(user, host:)
      super(user)

      @host = host
    end

    def call(command:)
      if (result = call_process(command:)).success?
        body = JSON.parse(result.body)
        if body["status"] == "OK" && body["exit_code"] == EXIT_CODE_SUCCESS
          body
        else
          raise StorageError, [body["error_message"], body["error_output"]].join(" ")
        end
      else
        raise StorageError, "Error while contacting rimrock (code #{result.status})"
      end
    end

    def call_process(command:)
      connection.post do |req|
        req.url "api/process"
        req.headers["Content-Type"] = "application/json"
        req.body = process_body(command)
      end
    end

    def process_body(command)
      {
        host: @host,
        command:
      }.to_json
    end
  end
end
