# frozen_string_literal: true

class PipelineUpdater
  def initialize(computation)
    @computation = computation
  end

  def call
    Pipelines::StartRunnableJob.perform_later(@computation.pipeline)
  end
end
