# frozen_string_literal: true

class GuardedExecutor
  def initialize(user)
    @user = user
  end

  def call
    @user.credentials_valid? ? block_given? && yield : report_problem
  end

  private
    def report_problem
      return unless notify_user?

      Notifier.credentials_expired(@user).deliver_later
      @user.update(credentials_expired_at: Time.zone.now)
    end

    def notify_user?
      @user.credentials_expired_at.blank? ||
        @user.credentials_expired_at < 1.day.ago
    end
end
