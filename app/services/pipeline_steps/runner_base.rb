# frozen_string_literal: true

module PipelineSteps
  class RunnerBase
    delegate :pipeline, :user, :pipeline_step, to: :computation
    attr_reader :computation

    def initialize(computation)
      @computation = computation
    end

    def call
      pre_internal_run
      updated = computation.update(status: :script_generated, started_at: Time.current)
      internal_run

      updated
    end

    protected
      def pre_internal_run; end

      def internal_run
        raise "This method should be implemented by descendent class"
      end
  end
end
