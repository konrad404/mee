# frozen_string_literal: true

class Step::ParametersComponent < ViewComponent::Base
  def initialize(step:)
    @parameters = step.parameters
  end

  def render?
    @parameters.present?
  end
end
