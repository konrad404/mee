# frozen_string_literal: true

class Patients::SummaryComponent < ViewComponent::Base
  with_collection_parameter :patient

  delegate :destroy?, to: :@policy

  def initialize(patient:, pundit_user:)
    @patient = patient
    @policy ||= Pundit.policy!(pundit_user, @patient)
  end
end
