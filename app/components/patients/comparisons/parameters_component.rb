# frozen_string_literal: true

class Patients::Comparisons::ParametersComponent < ViewComponent::Base
  def initialize(first:, second:)
    @first = first.index_by(&:key)
    @second = second.index_by(&:key)
  end

  def comparison
    (@first.keys + @second.keys).uniq.map do |key|
      Comparison.new(@first[key], @second[key])
    end
  end

  def render?
    @first.present? || @second.present?
  end

  private
    class Comparison
      def initialize(first, second)
        @first = first || NullParameterValue.new
        @second = second || NullParameterValue.new
      end

      def name
        @first.name || @second.name
      end

      def first
        @first.value
      end

      def second
        @second.value
      end
    end

    class NullParameterValue
      def name
        nil
      end

      def value
        "[undefined]"
      end
    end
end
