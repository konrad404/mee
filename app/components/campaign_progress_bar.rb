# frozen_string_literal: true

class CampaignProgressBar < ViewComponent::Base
  attr_reader :all, :campaign_progress_bar

  def initialize(campaign:)
    @all = campaign.pipelines.size
    @counts = campaign.pipelines
      .group_by(&:status)
      .transform_values(&:size)
    @campaign_progress_bar = Progress.new(title: tooltip_data,
    bars: [
      waiting_progress_bar,
      running_progress_bar,
      successful_progress_bar,
      failed_progress_bar
    ]
    )
  end

  def waiting_progress_bar
    ProgressBar.new(width: Percentage.new(number_of_waiting, all).to_f,
      color: "bg-primary", value_now: number_of_waiting)
  end

  def running_progress_bar
    ProgressBar.new(width: Percentage.new(number_of_running, all).to_f,
    color: "bg-warning", value_now: number_of_running)
  end

  def successful_progress_bar
    ProgressBar.new(width: Percentage.new(number_of_successful, all).to_f,
    color: "bg-success", value_now: number_of_successful)
  end

  def failed_progress_bar
    ProgressBar.new(width: Percentage.new(number_of_failed, all).to_f,
    color: "bg-danger", value_now: number_of_failed)
  end

  def number_of_successful
    @counts.fetch("success", 0)
  end

  def number_of_failed
    @counts.fetch("error", 0)
  end

  def number_of_waiting
    @counts.fetch("waiting", 0)
  end

  def number_of_running
    @counts.fetch("running", 0)
  end

  def tooltip_data
    I18n.t("progress_bar.data",
           waiting: number_of_waiting, running: number_of_running,
           succeeded: number_of_successful, failed: number_of_failed, all:)
  end
end
