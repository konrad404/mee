# frozen_string_literal: true

class StorageUsageBar < ViewComponent::Base
  attr_reader :storage_usage_progress_bar

  def initialize
    quota = Current.organization.quota
    color = quota.exceeded? ? "bg-danger" : "bg-primary"
    @storage_usage_progress_bar = Progress.new(
      title: "#{quota.usage}/#{quota.limit} GB",
      bars: [ProgressBar.new(width: quota.percentage_used,
           color:, value_now: quota.usage)]
    )
  end
end
