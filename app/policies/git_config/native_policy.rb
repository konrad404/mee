# frozen_string_literal: true

class GitConfig::NativePolicy < ApplicationPolicy
  def permitted_attributes
    [:git_type, :host, :download_key_file]
  end
end
