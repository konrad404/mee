# frozen_string_literal: true

PlgridPolicy = Struct.new(:user_context, :plgrid) do
  def show?
    plgrid_user?
  end

  def destroy?
    plgrid_user?
  end

  private
    def plgrid_user?
      user_context.user&.plgrid_login
    end
end
