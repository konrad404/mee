# frozen_string_literal: true

class ActiveStorage::AttachmentPolicy < ApplicationPolicy
  def destroy?
    owner_policy.new(user_context, owner).destroy?
  end

  def create?
    !Current.organization.quota.exceeded?
  end

  private
    def owner
      record.record
    end

    def owner_policy
      Pundit::PolicyFinder.new(owner).policy
    end
end
