# frozen_string_literal: true

class CohortPolicy < ApplicationPolicy
  class Scope < ApplicationPolicy::ApplicationScope
    def resolve
      scope.where(organization:)
    end
  end

  def index?
    true
  end

  def new?
    true
  end

  def create?
    in_organization?
  end

  def show?
    in_organization?
  end

  def destroy?
    in_organization?
  end

  def update?
    in_organization?
  end

  def permitted_attributes
    [:name]
  end
end
