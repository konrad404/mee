# frozen_string_literal: true

class LicensePolicy < ApplicationPolicy
  def index?
    admin?
  end

  def new?
    admin?
  end

  def create?
    admin? && in_organization?
  end

  def edit?
    admin? && in_organization?
  end

  def update?
    admin? && in_organization?
  end

  def destroy?
    admin? && in_organization?
  end

  def permitted_attributes
    [:name, :starts_at, :ends_at, config_attributes: [:key, :value]]
  end

  class Scope < ApplicationPolicy::ApplicationScope
    def resolve
      scope.where(organization: [organization, nil])
    end
  end
end
