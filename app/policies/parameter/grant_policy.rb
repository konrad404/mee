# frozen_string_literal: true

class Parameter::GrantPolicy < ApplicationPolicy
  def permitted_attributes
    [:id, :name, :hint]
  end
end
