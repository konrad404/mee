# frozen_string_literal: true

class Parameter::SelectPolicy < ApplicationPolicy
  def permitted_attributes
    [:type, :id, :name, :key, :hint, :default_value,
     :_destroy,
     values_attributes: [:name, :value]]
  end
end
