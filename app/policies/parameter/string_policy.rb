# frozen_string_literal: true

class Parameter::StringPolicy < ApplicationPolicy
  def permitted_attributes
    [:type, :id, :name, :hint, :key, :default_value, :_destroy]
  end
end
