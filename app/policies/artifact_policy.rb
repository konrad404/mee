# frozen_string_literal: true

class ArtifactPolicy < ApplicationPolicy
  def index?
    true
  end

  def new?
    !Current.organization.quota.exceeded?
  end

  def create?
    in_organization?
  end

  def edit?
    in_organization?
  end

  def update?
    in_organization?
  end

  def destroy?
    in_organization?
  end
end
