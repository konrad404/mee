# frozen_string_literal: true

class Storage::Config::PlgridPolicy < ApplicationPolicy
  def permitted_attributes
    [:type, :host, :host_key, :path]
  end
end
