# frozen_string_literal: true

class Patients::UploadJob < ApplicationJob
  def perform(uploader)
    Patients::Uploader.new(uploader).call
  end
end
