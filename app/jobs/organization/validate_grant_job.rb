# frozen_string_literal: true

class Organization::ValidateGrantJob < ApplicationJob
  def perform(organization)
    @organization = organization

    if no_grant_continuation?
      Notifier.grant_expired(organization: @organization).deliver_later
    end
  end

  private
    def no_grant_continuation?
      @organization.grants.where("ends_at > ?", Time.now + time_window).size.zero?
    end

    def time_window
      Mee::Application.config.checks.before_grant_expire
    end
end
