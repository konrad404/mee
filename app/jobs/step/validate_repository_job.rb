# frozen_string_literal: true

class Step::ValidateRepositoryJob < ApplicationJob
  def perform(step)
    Step::ValidateRepository.new(step).call
  end
end
