# frozen_string_literal: true

module Campaigns
  class CreatePipelineJob < ApplicationJob
    include ActionView::RecordIdentifier
    include Rails.application.routes.url_helpers
    include PipelineParams

    retry_on(PipelineCreationError, wait: 0, attempts: 5) do |job, error|
      campaign = job.arguments.first.fetch(:campaign)
      Rails.logger.error "Pipeline creation failed for #{campaign.inspect}"
      campaign.add_failed
    end

    def perform(patient:, user:, permitted_attributes:, steps_parameters_values:, campaign:)
      pipeline = ::Pipelines::Build.new(user, patient, permitted_attributes, steps_parameters_values, campaign:).call
      remove_blank_parameters_values(pipeline)
      finalize_pipeline_creation(user:, pipeline:, campaign:)
    end

    private
      def finalize_pipeline_creation(user:, pipeline:, campaign:)
        raise PipelineCreationError unless pipeline.save
        campaign.pipeline_created(pipeline)
      end
  end
end
