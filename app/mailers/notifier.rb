# frozen_string_literal: true

class Notifier < ApplicationMailer
  def account_approved(user:, organization:)
    @organization_name = organization.name
    @login_url = root_url(subdomain: organization.slug)

    mail(to: user.email, subject: I18n.t("emails.account_approved.subject",
                                         organization: @organization_name))
  end

  def credentials_expired(user)
    @proxy = user.proxy
    mail(to: user.email, subject: I18n.t("emails.credentials_expired.subject"))
  end

  def grant_expired(organization:)
    @name = organization.name
    admins_emails = organization.users.admins.pluck(:email)
    mail(to: admins_emails, subject: I18n.t("emails.grant_will_expire.subject"))
  end
end
