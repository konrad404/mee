# frozen_string_literal: true

class RunnableChannel < ApplicationCable::Channel
  def subscribed
    stream_for runnable if runnable
  end

  private
    def runnable
      Patient.find_by(slug: params[:runnable])
    end
end
