# frozen_string_literal: true

class Steps::GitConfigsController < ApplicationController
  before_action :find_and_authorize

  def index
    if override?
      render partial: "git_configs/form",
            layout: false,
            locals: {
              module_type: "step",
              git_config: Current.organization.git_config.class.new,
              git_config_url: step_git_configs_url(step_id: @step.id || "_new"),
              git_types: GitConfig.types
            }
    else
      render partial: "git_configs/default", layout: false
    end
  end

  def show
    git_config_class = GitConfig.by_type(params[:id])

    if git_config_class
      config = (@step&.git_config || git_config_class.new)
        .change_to_class(git_config_class)

      render partial: "git_configs/details",
            layout: false,
            locals: { git_config: config, module_type: "step" }
    else
      render json: { error: "Git config type not found" }, status: 404
    end
  end

  private
    def find_and_authorize
      @step = policy_scope(Step).find_by(id: params[:step_id]) || Step.new

      authorize(@step, @step.persisted? ? :update? : :create?)
    end

    def override?
      ActiveModel::Type::Boolean.new.cast(params[:override])
    end
end
