# frozen_string_literal: true

class Organizations::ArtifactsController < ApplicationController
  def new
    @file = ActiveStorage::Attachment.find(params[:file_id])
    @artifact = Artifact.new(attachment_id: params[:file_id], filename: @file.filename)
  end

  def create
    @artifact = Artifact.new(filename: params[:artifact][:filename], attachment_id: params[:file_id])
    @file = ActiveStorage::Attachment.find(params[:file_id])
    if @artifact.save
      flash.now[:notice] = t(".success")
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit
    @artifact = Artifact.find_by_id(params[:id])
  end

  def update
    @artifact = Artifact.find_by_id(params[:id])
    @file = @artifact.attachment
    if @artifact.update(filename: params.dig(:artifact, :filename))
      flash.now[:notice] = t(".success")
    else
      render :edit, status: :unprocessable_entity
    end
  end
end
