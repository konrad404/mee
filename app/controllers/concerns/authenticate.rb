# frozen_string_literal: true

module Authenticate
  extend ActiveSupport::Concern

  included do
    before_action :authenticate_user!

    prepend_before_action do
      Current.user = User.find_by(id: cookies.signed["user.id"]) if cookies.signed["user.id"]
      unless Current.user&.credentials_valid?
        session[:user_id] = nil
        Current.user = nil
      end
    end
  end

  private
    def authenticate_user!
      redirect_to root_path unless Current.user
    end
end
