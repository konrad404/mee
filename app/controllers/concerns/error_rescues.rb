# frozen_string_literal: true

module ErrorRescues
  extend ActiveSupport::Concern

  included do
    rescue_from ActiveRecord::RecordNotFound do
      redirect_back fallback_location: root_path,
                    alert: I18n.t("record_not_found")
    end

    rescue_from Organization::NotFoundError do
      redirect_to organization_not_found_url(subdomain: nil),
                  allow_other_host: true
    end

    rescue_from Membership::NotFoundError, Membership::BlockedError, Membership::NewError do
      redirect_to membership_path
    end

    rescue_from Pundit::NotAuthorizedError do |exception|
      redirect_back fallback_location: root_path,
                    alert: not_authorized_msg(exception)
    end
  end

  private
    def not_authorized_msg(exception)
      policy_name = exception.policy.class.to_s.underscore

      I18n.t("#{policy_name}.#{exception.query}",
             scope: "pundit", default: :default)
    end
end
