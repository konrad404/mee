# frozen_string_literal: true

module HelpItems
  extend ActiveSupport::Concern

  included do
    helper_method :help_items
  end

  def help_items
    policy_scope(HelpItem)
  end
end
