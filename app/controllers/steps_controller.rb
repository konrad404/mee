# frozen_string_literal: true

class StepsController < ApplicationController
  include Step::Params

  before_action :load_and_authorize, only: [:edit, :update, :destroy]

  def index
    authorize(Step)
    @pagy, @steps = pagy(policy_scope(Step).includes(:required_file_types, :parameters, :grant_type))
  end

  def new
    @step = Step.new
    authorize(@step)
  end

  def create
    @step = Step.new(step_params)
    authorize(@step)

    if @step.save
      redirect_to(steps_path)
    else
      render(:new, status: :unprocessable_entity)
    end
  end

  def edit
    @step = Current.organization.steps.includes(:persistent_errors).friendly.find(params[:id])
    authorize(@step)
    @step.load_persistent_errors
  end

  def update
    if @step.update(step_params(@step))
      redirect_to(steps_path)
    else
      @step.load_persistent_errors
      render(:edit, status: :unprocessable_entity)
    end
  end

  def destroy
    if @step.discard_or_destroy
      redirect_to(steps_path)
    else
      redirect_to steps_path, alert: I18n.t("steps.destroy.failure",
                                            flow_names: @step.flow_names)
    end
  end

  private
    def load_and_authorize
      @step = policy_scope(Step).friendly.find(params[:id])
      authorize(@step)
    end
end
