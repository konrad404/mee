# frozen_string_literal: true

class SessionsController < ActionController::Base
  skip_before_action :verify_authenticity_token, only: :create

  def create
    user = User.from_plgrid_omniauth(auth).tap { |u| u.save! }
    cookies.signed["user.id"] = user.id
    start_computations(user)

    redirect_to params[:origin] || root_path
  end

  def destroy
    cookies.delete("user.id")

    redirect_to root_path
  end

  private
    def auth
      request.env["omniauth.auth"]
    end

    def start_computations(user)
      Pipeline.automatic.where(user:).
        each { |p| Pipelines::StartRunnableJob.perform_later(p) }
    end
end
