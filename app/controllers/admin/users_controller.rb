# frozen_string_literal: true

module Admin
  class UsersController < ApplicationController
    before_action :find_and_authorize_user, except: :index

    def index
      authorize(Membership)

      @memberships = policy_scope(Membership).includes(:user)
      @new_users_count = @memberships.new_accounts.count
      @memberships = case state
                     when "active" then @memberships.approved
                     when "new" then @memberships.new_accounts
                     when "blocked" then @memberships.blocked
                     else @memberships
      end.order("users.last_name", "users.first_name")
      @pagy, @memberships = pagy(@memberships)
    end

    def destroy
      perform(t(".success", user: @membership.name)) do
        Membership::Destroy.new(Current.user, @membership).call
      end
    end

    def update
      attrs = state ? { state: } : permitted_attributes(Membership)

      perform(t(".success", user: @membership.name)) do
        Membership::Update.new(Current.user, @membership, attrs).call
      end
    end

    private
      def state
        params[:state]
      end

      def perform(ok_msg)
        case result = yield
        when :ok then redirect_to(admin_users_path, notice: ok_msg)
        else redirect_to(admin_users_path, alert: t(".#{result}"))
        end
      end

      def find_and_authorize_user
        @membership = Membership.includes(:user).find(params[:id])
        authorize(@membership)
      end
  end
end
