# frozen_string_literal: true

class Pipelines::FileBrowsersController < ApplicationController
  def show
    @pipeline = Pipeline.includes(:runnable).find(params[:pipeline_id])
    @runnable = @pipeline.runnable
    authorize(@pipeline)
  end
end
