# frozen_string_literal: true

class MembershipsController < ApplicationController
  skip_before_action :authorize_membership!
  layout "memberships"

  def show
    if Current.membership.nil?
      render :not_a_member
    elsif Current.membership.blocked?
      render :blocked
    elsif Current.membership.new_account?
      render :new_account
    else
      redirect_to root_path
    end
  end

  def create
    if Membership.create(user: Current.user, organization:)
      flash.now[:notice] = "Join request created"
      render :new_account
    else
      flash.now[:error] = "Unable to create join request"
      render :not_a_member
    end
  end

  def update
    @membership = Current.membership
    if @membership.update_attribute(:dataverse_token, params[:membership][:dataverse_token])
      flash[:notice] = I18n.t("profiles.show.dataverse.updated")
      redirect_to profile_path
    end
  end
end
