# frozen_string_literal: true

class FlowsController < ApplicationController
  before_action :load_and_authorize, only: [:update, :destroy]

  def index
    authorize(Flow)
    @pagy, @flows = pagy(policy_scope(Flow).includes(:steps))
  end

  def new
    @flow = Flow.new
    authorize(@flow)

    @steps = policy_scope(Step)
  end

  def create
    @flow = Flow.new(permitted_attributes(Flow))
    authorize(@flow)

    if @flow.save
      redirect_to(flows_path)
    else
      @steps = policy_scope(Step)
      render(:new, status: :unprocessable_entity)
    end
  end

  def edit
    @flow = Current.organization.flows.includes(flow_steps: :step).find(params[:id])
    authorize(@flow)

    @steps = policy_scope(Step)
  end

  def update
    if @flow.update(permitted_attributes(@flow))
      redirect_to(flows_path)
    else
      @steps = policy_scope(Step)
      render(:edit, status: :unprocessable_entity)
    end
  end

  def destroy
    @flow.discard_or_destroy
    redirect_to(flows_path)
  end

  private
    def load_and_authorize
      @flow = Current.organization.flows.find(params[:id])
      authorize(@flow)
    end
end
