# frozen_string_literal: true

class Patients::FilesController < ApplicationController
  def create
    patient = Patient.find_by(slug: params[:patient_id])
    patient.add_input(params[:patient][:files])
    redirect_back fallback_location: root_path, notice: t(".success")
  rescue StandardError
    redirect_back fallback_location: root_path, alert: t(".failure")
  end
end
