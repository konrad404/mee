# frozen_string_literal: true

class Computations::ActionsController < ApplicationController
  def show
    @computation = Computation.find(params[:computation_id])
    @pipeline = @computation.pipeline

    authorize(@computation, :show?)
  end
end
