# frozen_string_literal: true

class Computations::ArtifactsController < Computations::ApiController
  def show
    if artifact = Current.organization.artifacts_blobs.find_by(filename: params[:name])
      redirect_to url_for(artifact)
    else
      render nothing: true, status: :not_found
    end
  end

  def create
    if params[:file].nil?
      render nothing: true, status: :bad_request
      return
    end
    if Artifact.upload(files: [params[:file]])
      render nothing: true, status: :created
    else
      render nothing: true, status: :internal_server_error
    end
  end
end
