# frozen_string_literal: true

class Computations::FilesController < Computations::ApiController
  def show_by_type
    serve_file(@computation.pick_file_by_type(params[:type]))
  end

  def show_by_filename
    serve_file(@computation.pick_file_by_filename(params[:filename]))
  end

  def create
    if params[:file].nil?
      render nothing: true, status: :bad_request
      return
    end

    if @computation.add_output(params[:file])
      render nothing: true, status: :created
    else
      render nothing: true, status: :internal_server_error
    end
  end

  def serve_file(file)
    if file
      redirect_to url_for(file)
    else
      render nothing: true, status: :not_found
    end
  end
end
