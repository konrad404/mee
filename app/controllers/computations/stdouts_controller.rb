# frozen_string_literal: true

class Computations::StdoutsController < ApplicationController
  def show
    @computation = Computation.find(params[:computation_id])
    authorize(@computation, :show?)

    if @computation.stdout.attached?
      redirect_to rails_blob_path(@computation.stdout)
    else
      render plain: @computation.site
        .fetch_logs(@computation.stdout_path, Current.user).read
    end
  end
end
