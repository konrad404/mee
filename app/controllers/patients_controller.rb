# frozen_string_literal: true

class PatientsController < ApplicationController
  before_action :set_patients_and_uploader, only: [:index]
  before_action :find_and_authorize, only: [:show, :destroy]

  def index
    authorize(Patient)
    @pagy, @patients = pagy(@patients)
  end

  def show
    @pipelines = @patient.pipelines.
      includes(:user, :flow, computations: [:step, :pipeline])

    @pagy, @pipelines = pagy(filter(@pipelines))

    if request.xhr?
      render(partial: "runnables/pipelines/list", layout: false,
             locals: { runnable: @patient, pipelines: @pipelines })
    end
  end

  def new
    @patient = Patient.new
    authorize(@patient)
  end

  def create
    @patient = Patient.new(permitted_attributes(Patient))
    if @patient.save
      redirect_to @patient, notice: I18n.t("patients.create.success")
    else
      render :new, status: :unprocessable_entity
    end
  end

  def destroy
    if @patient.destroy
      redirect_to patients_path,
                  notice: I18n.t("patients.destroy.success", case_number: @patient.case_number),
                  status: :see_other
    else
      render :show,
             notice: I18n.t("patients.destroy.failure",
                            case_number: @patient.case_number)
    end
  end

  private
    def filter(scope)
      scope = scope.where("pipelines.name LIKE ?", "%#{params[:name].strip}%") if params[:name].present?
      scope = scope.where(user_id: params[:owner]) if params[:owner].present?
      scope = scope.where(pipelines: { status: params[:status] }) if params[:status].present?

      scope
    end

    def new_patient
      Patient.new(permitted_attributes(Patient))
    end

    def set_patients_and_uploader
      @patients = policy_scope(Patient).includes(pipelines: { computations: :step }).all
      @uploader = Patient::Uploader.recent.where(cohort: nil).first
    end

    def find_and_authorize
      @patient = policy_scope(Patient).find_by!(slug: params[:id])
      authorize(@patient)
    end
end
