# frozen_string_literal: true

module GrantsHelper
  def grant_types_names(grant)
    grant.grant_types.map { |g| g[:name] }
  end
end
