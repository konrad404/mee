# frozen_string_literal: true

module PipelineHelper
  def pipeline_title(pipeline)
    pipeline_icon(pipeline) + " " +
      pipeline.name + " " +
      content_tag(:small,
                  pipeline.flow.name +
                  " " +
                  pipeline_type(pipeline),
                  class: "text-muted d-none d-md-inline-block")
  end

  def pipeline_owner(pipeline)
    content_tag(:div,
                I18n.t("pipelines.show.owner", owner: pipeline.owner_name),
                class: "label label-primary owner-label")
  end

  private
    def pipeline_icon(pipeline)
      classes =
        if pipeline.manual?
          %w[far fa-hand-paper]
        else
          if pipeline.running?
            %w[fas fa-cog fa-spin]
          elsif pipeline.campaign_id
            %w[fas fa-layer-group]
          else
            %w[fas fa-cog]
          end
        end
      tag.i(class: classes)
    end

    def pipeline_type(pipeline)
      I18n.t("pipelines.show.subtitle", type:  pipeline.campaign_id ? "campagin" : pipeline.mode)
    end
end
