# frozen_string_literal: true

module FileableHelper
  def file_upload_path(fileable)
    url_for [fileable, :inputs]
  end
end
