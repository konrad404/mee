# frozen_string_literal: true

module FlowsHelper
  def steps_list(form, flow, steps, html_options = {})
    current_steps = flow.flow_steps.index_by(&:step_id)
    flow_steps = steps
      .map { |s| current_steps[s.id] || FlowStep.new(step: s) }

    options = flow_steps.map do |fs|
      o = { value: fs.step.id }
      o[:selected] = "selected" if fs.flow
      content_tag(:option, fs.step.name, o)
    end

    templates = flow_steps.to_h { |fs| ["data-#{fs.step.id}", step_template(form, fs)] }

    [
      content_tag(:div, "", templates
        .merge(class: "d-none", "data-flow-steps-target": "templates")),
      select_tag("steps_list", options.join("\n").html_safe,
                html_options.merge(multiple: true))
    ].join("\n").html_safe
  end

  def step_template(form, flow_step)
    render(partial: "flows/flow_step",
           locals: { form:, flow_step: })
  end
end
