# frozen_string_literal: true

module RunnableHelper
  def runnable_path(runnable)
    runnable.class == Patient ? patient_path(runnable) : pipelines_path
  end

  def new_runnable_pipeline_path(runnable)
    runnable.class == Patient ? new_patient_pipeline_path(runnable) : new_pipeline_path
  end

  def runnable_comparisons_header(runnable, first_pipe, second_pipe)
    render "comparisons/#{runnable.class.model_name.singular}_header",
           runnable:, first_pipe:, second_pipe:
  end

  def runnable_pipeline_header(runnable, pipeline)
    render "runnables/pipelines/#{runnable.class.model_name.singular}_pipelines_list_header",
           runnable:, pipeline:
  end

  def runnable_header(runnable, pipeline)
    render "runnables/#{runnable.class.model_name.singular}_header",
           runnable:, pipeline:
  end

  def runnable_simple_form_for(runnable, pipeline)
    render "runnables/pipelines/#{runnable.class.model_name.singular}_pipeline_form",
           runnable:, pipeline:
  end
end
