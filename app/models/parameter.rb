# frozen_string_literal: true

class Parameter < ApplicationRecord
  base_class

  validates :key, presence: true
  # This does not work when 2 or more parameters are created at once with the
  # same key value. See https://github.com/rails/rails/issues/20676 for
  # details.
  # , uniqueness: { scope: :step }
  #
  validate do
    if step && step.parameters.map(&:key).count(key) > 1
      errors.add(:key, :taken)
    end
  end

  validates :name, presence: true

  belongs_to :step
  has_many :parameter_values, dependent: :nullify

  def value_class
    "ParameterValue::#{self.class.name.demodulize}".constantize
  end

  def type_name
    self.class.type_name
  end

  def protected?
    false
  end

  class << self
    def by_type(type_name)
      types_classes.find { |type| type.type_name == type_name }
    end

    def types
      types_classes.map(&:type_name)
    end

    def type_name
      model_name.element
    end

    private
      def types_classes
        [Parameter::Select, Parameter::String, Parameter::Number]
      end
  end
end
