# frozen_string_literal: true

require "friendly_id_error_mover"

class Patient < ApplicationRecord
  include OrganizationUnit, Fileable::FileHandler
  extend FriendlyId
  friendly_id :case_number, use: [:scoped, FriendlyIdErrorMover], scope: :organization

  has_many_attached :inputs
  has_many :pipelines,
             -> { order(iid: :asc) },
             inverse_of: "runnable",
             dependent: :destroy,
             as: :runnable
  has_many :cohort_patients, dependent: :destroy
  has_many :cohorts, through: :cohort_patients

  validates :case_number, presence: true, uniqueness: { scope: :organization_id, case_sensitive: false }

  default_scope { order("case_number asc") }

  def to_param
    slug
  end

  def pick_file_by_type(type)
    inputs_blobs.order(:created_at).select { |blob| blob.metadata["file_type"] == type }.first
  end

  def pick_file_by_filename(filename)
    inputs_blobs.find_by(filename:)
  end

  def status
    pipelines.last&.status
  end

  def start_computations
    pipelines.where(campaign: nil).find_each do |pipeline|
      Pipelines::StartRunnableJob.perform_later(pipeline)
    end
  end

  def files
    ActiveStorage::Attachment.where(record_id: pipelines.pluck(:id), record_type: "Pipeline")
    .or(ActiveStorage::Attachment.where(record_id: id, record_type: "Patient"))
  end
end
