# frozen_string_literal: true

module Computation::Broadcaster
  extend ActiveSupport::Concern
  include ActionView::RecordIdentifier

  included do
    after_update do
      if previous_changes["status"] && pipeline.campaign.nil?
        Current.organization = pipeline.runnable.organization
        broadcast_status
        broadcast_show
      end
    end
  end

  def broadcast_status
    Turbo::StreamsChannel.broadcast_replace_to dom_id(pipeline, "show"),
    target: dom_id(self, "status_component"),
    html: ApplicationController.render(
      Computation::StatusComponent.new(computation: self),
      layout: false
    )
  end

  def broadcast_show
    Turbo::StreamsChannel.broadcast_replace_to dom_id(pipeline, "show"),
    target: dom_id(self, "show"),
    html: ActionController::Renderer.new(
            ApplicationController, {},
            { script_name: "/#{OrganizationSlug.encode(pipeline.runnable.organization_id)}" }
          ).render(partial: "computations/show",
            locals: { pipeline:,
            computation: self, computations: pipeline.computations },
            layout: false
          )
  end
end
