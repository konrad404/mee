# frozen_string_literal: true

class ParameterValue::Number < ParameterValue
  store_accessor :value_store, :value

  attribute :value, :integer

  validates :value, numericality: true, if: :min_max_not_present?
  validates :value, numericality: {
    greater_than_or_equal_to: ->(p) { p.parameter.min }
  }, if: :min_present?
  validates :value, numericality: {
    less_than_or_equal_to: ->(p) { p.parameter.max }
  }, if: :max_present?

  def permitted_params
    [:value]
  end

  def blank?
    value.blank?
  end

  def value
    super || parameter&.default_value
  end

  private
    def min_present?
      parameter&.min.present?
    end

    def max_present?
      parameter&.max.present?
    end

    def min_max_not_present?
      !(min_present? && max_present?)
    end
end
