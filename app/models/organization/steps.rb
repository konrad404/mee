# frozen_string_literal: true

module Organization::Steps
  extend ActiveSupport::Concern

  included do
    has_many :steps, dependent: :destroy
  end
end
