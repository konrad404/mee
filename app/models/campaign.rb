# frozen_string_literal: true

class Campaign < ApplicationRecord
  include ActionView::RecordIdentifier
  include Campaign::Broadcaster

  belongs_to :cohort, counter_cache: :campaigns_count, optional: true
  has_many :pipelines, dependent: :destroy

  belongs_to :organization, default: -> { Current.organization }
  attr_accessor :flow

  before_create do
    self.desired_pipelines = cohort.patients.size
  end

  after_touch :update_status!

  enum status: {
    initializing: 0,
    created: 1,
    running: 2,
    finished: 3,
    creating_pipelines_failed: 4
  }

  validates :name, presence: true,
            uniqueness: { scope: :cohort_id, case_sensitive: true },
            format: { with: /\A[a-zA-Z0-9_]+\z/,
                      message: I18n.t("campaigns.errors.name") }
  validates_each :cohort, on: :create do |record, attr, cohort|
    record.errors.add(attr, I18n.t("campaigns.errors.cohort_doesnt_exist")) unless cohort
    record.errors.add(attr, I18n.t("campaigns.errors.empty_cohort")) unless cohort&.patients.present?
  end

  validates_each :flow, on: :create do |record, attr, flow|
    record.errors.add(attr, I18n.t("campaigns.errors.cohort_doesnt_exist")) unless Flow.find_by(id: flow)
  end

  def run
    pipelines.each { |pipeline| ::Pipelines::StartRunnableJob.perform_later(pipeline) }
    running!
  end

  def update_status!
    # TODO Remove if when we upgrade to Rails 7.1 https://github.com/rails/rails/pull/46522)
    new_status = Campaign::StatusCalculator.new(self).calculate
    update(status: new_status) if new_status != status.to_sym
  end

  def add_failed
    increment!(:failed_pipelines)
    update_status!
  end
end
