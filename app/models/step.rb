# frozen_string_literal: true

require "friendly_id_error_mover"

class Step < ApplicationRecord
  include Discard::Model
  include Git, OrganizationUnit, Errorable, Parameters

  extend FriendlyId
  friendly_id :name, use: [:scoped, FriendlyIdErrorMover], scope: :organization

  belongs_to :grant_type
  belongs_to :user, optional: true, default: -> { Current.user }
  belongs_to :site

  has_many :prerequisites, dependent: :destroy
  has_many :required_file_types, through: :prerequisites, source: :data_file_type

  has_many :flow_steps, dependent: :destroy
  has_many :flows, through: :flow_steps

  has_many :computations, dependent: :destroy

  validates :name,
    presence: true,
    uniqueness: { scope: :organization_id, case_sensitive: false }

  validates :slug,
    uniqueness: { scope: :organization_id, case_sensitive: false }

  def self.update_counters(id, counters)
    if diff = counters["persistent_errors_count"]
      org_id = Organization.joins(:steps).where(steps: { id: }).pluck(:id).first
      Organization.update_counters(org_id, "steps_persistent_errors_count" => diff)
    end

    super(id, counters)
  end

  def discard_or_destroy
    return false if has_active_flows?

    flows.any? ? discard : destroy
  end

  def has_active_flows?
    flows.kept.any?
  end

  def destroyable?(flow)
    discarded? && computations.empty? && flows.size == 1 && flows.first == flow
  end

  def flow_names
    flows.map { |flow| flow.name }.join("\n")
  end

  def input_present_for?(pipeline)
    required_file_types.map(&:data_type)
      .map { |file_type| pipeline.data_file(file_type) }.all?
  end

  def active_grants
    organization.grants.active.joins(:grant_types).where(grant_types: { id: grant_type.id })
  end

  def name=(name)
    super(name&.strip)
  end

  def validate_later
    Step::ValidateRepositoryJob.perform_later(self)
  end
end
