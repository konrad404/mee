# frozen_string_literal: true

class Computation < ApplicationRecord
  include ParameterValues
  include DataFiles
  include SecretControl
  include FileHandler
  include Broadcaster

  attr_accessor :internal_errors

  belongs_to :user
  belongs_to :pipeline, touch: true
  belongs_to :step

  has_one_attached :stdout, service: :local
  has_one_attached :stderr, service: :local

  enum status: {
    created: 0,
    script_generated: 1,
    queued: 2,
    running: 3,
    finished: 4,
    aborted: 5,
    error: 6
  }

  validate do
    errors.merge!(internal_errors) if internal_errors
  end
  validates :script, presence: { message: "Validation error, some errors are occurring, check the configuration" }, unless: :created?

  scope :active, -> { where(status: %w[script_generated queued running]) }
  scope :submitted, -> { where(status: %w[queued running]) }
  scope :created, -> { where(status: "created") }
  scope :not_finished, -> { where(status: %w[created script_generated queued running]) }
  scope :for_patient_status, ->(status) { where(pipeline_step: status) }

  delegate :mode, :manual?, :automatic?, to: :pipeline
  delegate :name, :site, :organization, to: :step
  delegate :repository, :file, :host, :download_key, to: :step

  before_update :remove_secret, if: :status_changed?

  def initialize(attrs = {})
    if attrs
      step = attrs[:step]
      pipeline = attrs[:pipeline]

      attrs[:user] ||= pipeline&.user
      attrs[:pipeline_step] = step&.name
    end

    super(attrs)
  end

  def active?
    %w[script_generated queued running].include? status
  end

  def completed?
    %w[error finished aborted].include? status
  end

  def run
    site.run(self)
  end

  def abort!
    site.abort(self)
  end

  def runnable?
    !pipeline.archived? && step.input_present_for?(pipeline) && previous_accounted?
  end

  def previous_accounted?
    previous_computation_dependency? ? previous_computations_completed? : true
  end

  def configured?
    tag_or_branch.present?
  end

  def success?
    status == "finished"
  end

  def error?
    status == "error"
  end

  def created?
    status == "created"
  end

  def computed_status
    if success?
      :success
    elsif error?
      :error
    elsif active?
      :running
    else
      :waiting
    end
  end

  def site_host
    site.host
  end

  def fetch_logs
    stdout.attach(io: site.fetch_logs(stdout_path, user), filename: "slurm.out")
    stderr.attach(io: site.fetch_logs(stderr_path, user), filename: "slurm.err")
  end

  def belongs_to_campaign?
    !pipeline.campaign.nil?
  end

  def tag_or_branch
    parameter_value_for(Step::Parameters::TAG_OR_BRANCH)&.value
  end

  private
    def submitted?
      %w[queued running].any?(status)
    end

    def previous_computation_dependency?
      FlowStep.find_by(flow_id: pipeline.flow.id, step_id:).wait_for_previous
    end

    def previous_computations_completed?
      # computations are created according to flow_step.position order (see pipelines/build.rb)
      # so computations created earlier also are before current computation in the pipeline
      pipeline.computations.where("created_at < ?", created_at).all?(&:finished?)
    end
end
