# frozen_string_literal: true

class Liquid::DataverseEntry
  private attr_reader :context
  attr_reader :doi
  attr_reader :token

  def initialize(liquid_context, doi)
    @context = liquid_context
    @doi = doi
    @dataverse_url, @token = get_dataverse_config
  end

  def organization
    context.registers[:computation].organization
  end

  def membership
    context.registers[:computation].user.memberships.detect { |m| m.organization_id == organization.id }
  end

  def dataset_url
    "#{@dataverse_url}/api/access/dataset/:persistentId/?persistentId=#{@doi}"
  end

  def datafile_url
    "#{@dataverse_url}/api/access/datafile/:persistentId/?persistentId=#{@doi}"
  end

  def add_datafile_url
    "#{@dataverse_url}/api/datasets/:persistentId/add?persistentId=#{@doi}"
  end

  def get_dataverse_config
    dataverse_url = organization.dataverse_url
    token = membership.dataverse_token

    if dataverse_url.blank?
      context.registers[:errors]
             .add(:script, I18n.t("dataverse.missing_dataverse_url"))
    end
    if token.blank?
      context.registers[:errors]
             .add(:script, I18n.t("dataverse.missing_token"))
    end
    [dataverse_url, token]
  end
end
