# frozen_string_literal: true

class Site < ApplicationRecord
  with_options presence: true do
    validates :name, uniqueness: true
    validates :host
    validates :host_key
  end

  def abort(computation, options = {})
    Rimrock::Abort.new(computation, PipelineUpdater, options).call
  end

  def run(computation)
    PipelineSteps::Rimrock::Runner.new(computation).call
  end

  def fetch_logs(source_path, user)
    StringIO.new(content_for(source_path, user.proxy.encode) || "")
  end

  private
    def content_for(path, proxy)
      plgdata ||= ProxyConnection.build("https://data.plgrid.pl", proxy)
      result = plgdata.get("/download/#{host_key}/#{path}")
      result.body if result.success?
    end
end
