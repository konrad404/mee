# frozen_string_literal: true

class GrantType < ApplicationRecord
  has_many :steps, dependent: :restrict_with_error
  has_many :grant_typings, dependent: :destroy
  has_many :grants, through: :grant_typings

  validates :name, presence: true, uniqueness: { case_sensitive: false }
end
