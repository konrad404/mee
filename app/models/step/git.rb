# frozen_string_literal: true

module Step::Git
  extend ActiveSupport::Concern
  include GitAttributes

  included do
    serialize :git_config, GitConfig
    validates_associated :git_config

    store_accessor :repo_config, :repository, :file
    validates :repository, presence: true
    validates :file, presence: true
  end

  def host
    calculated_git_config.host
  end

  def download_key
    calculated_git_config.download_key
  end

  def repository
    repo_config.repository
  end

  def file
    repo_config.file
  end

  def template_and_revision_for(tag_or_branch)
    git_repository.content_and_revision_for(file, tag_or_branch)
  end

  def git_repository
    calculated_git_config.git_repository(repository_path: repository)
  end

  def calculated_git_config
    git_config || organization.git_config
  end

  def versions(force_reload: false)
    versions = git_repository.versions(force_reload:)
    { branches: versions[:branches], tags: versions[:tags] } if versions
  end

  def default_branch
    git_repository.versions[:default_branch] if git_repository.versions
  end
end
