# frozen_string_literal: true

class Pipeline < ApplicationRecord
  include ActionView::RecordIdentifier, Fileable::FileHandler
  include Broadcaster

  enum mode: [:automatic, :manual]
  has_rich_text :notes
  has_many_attached :inputs
  has_many_attached :outputs
  belongs_to :runnable, polymorphic: true

  belongs_to :user
  belongs_to :flow

  belongs_to :campaign, optional: true, touch: true

  has_many :computations,
           # computations are created according to flow_step.position order (see pipelines/build.rb)
           # so this scope returns computations in order steps were put in the flow
           -> { order(:created_at) },
           dependent: :destroy

  validate :set_iid, on: :create
  validates :iid, presence: true, numericality: true
  validates :name, presence: true
  validates :mode, presence: true
  validates_associated :computations, on: :create

  scope :automatic, -> { where(mode: :automatic) }
  scope :latest, ->(nr = 3) { reorder(created_at: :desc).limit(nr) }

  delegate :steps, :organization, to: :flow

  after_destroy { |pipe| pipe.flow.destroy unless pipe.flow.kept? || pipe.flow.used? }

  after_touch do
    update_status!
  end

  enum status: {
    waiting: 0,
    running: 1,
    success: 2,
    error: 3
  }

  def pick_file_by_type(type)
    if organization.data_file_types.find_by(data_type: type)
      outputs_blobs.order(:created_at).select { |blob| blob.metadata["file_type"] == type }.first ||
      inputs_blobs.order(:created_at).select { |blob| blob.metadata["file_type"] == type }.first ||
      runnable.pick_file_by_type(type)
    end
  end

  def pick_file_by_filename(filename)
    inputs_blobs.find_by(filename:) ||
    outputs_blobs.find_by(filename:) ||
    runnable.pick_file_by_filename(filename)
  end

  def data_file(data_type)
    data_files(data_type).first
  end

  def data_files(data_type)
    (inputs_blobs + outputs_blobs + runnable_blobs)
      .select { |blob| blob.metadata[:file_type] == data_type }
  end

  def runnable_blobs
    runnable&.inputs_blobs
  end

  def update_status!
    # TODO Remove if when we upgrade to Rails 7.1 https://github.com/rails/rails/pull/46522)
    new_status = Pipeline::StatusCalculator.new(self).calculate
    update(status: new_status) if new_status != status.to_sym
  end

  def owner_name
    user&.name || "(deleted user)"
  end

  def archived?
    flow.discarded?
  end

  def start_computations
    Pipelines::StartRunnableJob.perform_later(self) unless campaign
  end

  private
    def set_iid
      self.iid = runnable.pipelines.maximum(:iid).to_i + 1 if iid.blank?
    end
end
