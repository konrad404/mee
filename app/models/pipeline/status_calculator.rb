# frozen_string_literal: true

class Pipeline::StatusCalculator
  STATUSES = [:success, :error, :running, :waiting]

  def initialize(pipeline)
    @pipeline = pipeline
  end

  def calculate
    if statuses["error"] > 0
      :error
    elsif statuses["active"] > 0
      :running
    elsif statuses["success"] == @pipeline.computations.size
      :success
    else
      :waiting
    end
  end

  private
    def statuses
      @statuses ||= Computation.where(pipeline: @pipeline).select(:status).group(:status).count.tap do |statuses|
        statuses.default = 0
        statuses["active"] = statuses["script_generated"] + statuses["queued"] + statuses["running"]
        statuses["success"] = statuses["finished"]
      end
    end
end
