# frozen_string_literal: true

module Pipeline::Broadcaster
  extend ActiveSupport::Concern
  include ActionView::RecordIdentifier

  included do
    after_update do
      if status_previously_changed? && campaign
        broadcast_status
      end
    end
  end

  def broadcast_status
    broadcast_replace target: dom_id(runnable),
    partial: "campaigns/patient_table/pipeline_row",
    locals: { pipeline: self }
  end

  def broadcast_file_browsers
    Turbo::StreamsChannel.broadcast_replace_to dom_id(self, "show"),
    target: dom_id(self, "file_browsers"),
    html: ActionController::Renderer.new(
            ApplicationController, {},
            { script_name: "/#{OrganizationSlug.encode(runnable.organization_id)}" }
          ).render(partial: "pipelines/file_browsers",
            locals: { pipeline: self, runnable: self.runnable },
            layout: false
          )
  end
end
