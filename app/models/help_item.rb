# frozen_string_literal: true

class HelpItem
  SEPARATOR = "/"
  ITEMS = {
    %w[manual step] => nil,
    %w[manual patient_upload] => nil,
    %w[manual dataverse] => nil
  }.freeze

  def initialize(category, file, required_role)
    @category = category
    @file = file
    @required_role = required_role
  end

  attr_reader :category, :file

  def title
    I18n.t("help.#{category}.#{file}.title")
  end

  def description
    I18n.t("help.#{category}.#{file}.description")
  end

  class << self
    def for(category, file)
      all.find do |item|
        item.category == category && item.file == file
      end || raise(ArgumentError)
    end

    def all
      @all ||= ITEMS.map do |path, required_role|
        category, file = path
        new(category, file, required_role)
      end
    end
  end

  def enabled_for?(organization)
    !@required_role || organization.has_role?(@required_role)
  end

  def path
    Rails.root.join("doc", category, "#{file}.md")
  end
end
