# frozen_string_literal: true

class ParameterValue < ApplicationRecord
  base_class

  validates :key, presence: true
  validates :name, presence: true

  belongs_to :computation
  belongs_to :parameter, optional: true

  delegate :hint, :step, to: :parameter, allow_nil: true

  def initialize(attrs = {})
    parameter = attrs[:parameter]
    attrs[:key] ||= parameter&.key
    attrs[:name] ||= parameter&.name

    super(attrs)
  end
end
