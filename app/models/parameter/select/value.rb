# frozen_string_literal: true

class Parameter::Select::Value
  include ActiveModel::Model
  include ActiveModel::Attributes

  attribute :name, :string
  attribute :value, :string

  validates :name, presence: true
  validates :value, presence: true

  validate do
    if select && select.values.map(&:value).count(value) > 1
      errors.add(:value, :taken)
    end

    if select && select.values.map(&:name).count(name) > 1
      errors.add(:name, :taken)
    end
  end

  attr_accessor :select

  def name=(name)
    super(name.strip)
  end

  def value=(value)
    super(value.strip)
  end

  def persisted?
    false
  end
end
