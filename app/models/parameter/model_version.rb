# frozen_string_literal: true

class Parameter::ModelVersion < Parameter
  delegate :versions, to: :step

  def protected?
    true
  end

  def default_value
    step.default_branch
  end
end
