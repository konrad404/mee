# frozen_string_literal: true

class Prerequisite < ApplicationRecord
  belongs_to :step
  belongs_to :data_file_type
end
