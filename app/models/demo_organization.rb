# frozen_string_literal: true

class DemoOrganization
  include ActiveModel::API
  include ActionDispatch::TestProcess::FixtureFile

  attr_accessor :name, :logo_path, :ssh_key_path,
                :gitlab_api_private_token,
                :plgrid_team_id, :grants

  validates :name, presence: true
  validates :ssh_key_path, presence: true
  validates :gitlab_api_private_token, presence: true
  validates :plgrid_team_id, presence: true

  def self.create!(params)
    self.new(params).save!
  end

  def save!
    org = Organization.find_by(name:)
    if org
      STDOUT.puts "#{name} organization already exits. Do you want to reset it [y/n]"
      return unless STDIN.gets.chomp == "y"

      org.destroy!
    end

    Organization.transaction do
      create_organization.tap do |org|
        types = create_data_types(org)
        create_grants(org)
        create_flows(org, types)
        create_patients(org)
        create_pipelines(org)
        create_artifacts(org)
        create_cohort(org)
      end
    end
  end

  private
    def create_organization
      download_key = File.read(ssh_key_path)
      git_config = GitConfig::Gitlab.new(host: "gitlab.com",
                                        private_token: gitlab_api_private_token,
                                        download_key:)

      Organization.create!(name:,
                          git_config:,
                          plgrid_team_id:).
      tap do |org|
        User.admins.each do |user|
          Membership.create!(user:, organization: org, state: :approved, roles: [:admin])
        end

        if logo_path && File.exist?(logo_path)
          org.logo.attach(io: File.open(logo_path), filename: File.basename(logo_path))
        end
      end
    end

    def create_data_types(org)
      [
        [:demo_numbers, /^numbers.*\.txt$/, "text", "Numbers to be sorted"],
        [:demo_steps, /^steps.*\.txt$/, "text", "Sorting steps"],
        [:demo_animation, /^plot.*\.gif$/, "graphics", "Sorting animation"]
      ].to_h do |record|
        dft = DataFileType.find_or_initialize_by(data_type: record[0], organization: org)
        dft.pattern = record[1].source
        dft.name = record[3]
        dft.viewer = record[2]
        dft.save!

        [record[0], dft]
      end
    end

    def create_grants(org)
      (grants || []).map do |name, start_date|
        Grant.create!(organization: org, name:,
                      starts_at: start_date, ends_at: start_date + 1.year,
                      grant_types: [cpu_grant_type, gpu_grant_type])
      end
    end

    def create_site
      Site.find_or_create_by!(name: "ares",
                              host: "ares.cyfronet.pl", host_key: "ares")
      Site.find_or_create_by!(name: "athena",
                              host: "athena.cyfronet.pl", host_key: "athena")
    end

    def create_flows(org, types)
      owner = org.users.first
      site = Site.find_by host_key: "ares"

      Step.create!(name: "Generate numbers",
        file: "0_generate_input.job.bash.liquid",
        repository: "cyfronet/mee-demo-steps",
        organization: org, grant_type: cpu_grant_type,
        user: owner, site:)
      Step.create!(name: "Sort numbers",
        file: "1_sort.job.bash.liquid",
        repository: "cyfronet/mee-demo-steps",
        required_file_types: [types[:demo_numbers]],
        organization: org, grant_type: cpu_grant_type,
        user: owner, site:)
      Step.create!(name: "Generate animation",
        file: "2_visualization.job.bash.liquid",
        repository: "cyfronet/mee-demo-steps",
        required_file_types: [types[:demo_steps]],
        organization: org, grant_type: cpu_grant_type,
        user: owner, site:)

      Flow.create!(name: "Demo with number generation",
                  position: 2, organization: org, user: owner, flow_steps_attributes: [
                    { step: Step.first, position: 1 },
                    { step: Step.second, position: 2 },
                    { step: Step.third, position: 3 }
                  ])

      Flow.create!(name: "Demo without number generation",
                  position: 1, organization: org, user: owner,
                  flow_steps_attributes: [
                    { step: Step.second, position: 1 },
                    { step: Step.third, position: 2 }
                  ])
      create_artifact_flow(org)
    end

    def create_artifact_flow(org)
      owner = org.users.first
      step = Step.create!(name: "Sort artifact numbers",
        file: "1_artifact_sort.job.bash.liquid",
        repository: "cyfronet/mee-demo-steps",
        organization: org, grant_type: cpu_grant_type,
        user: owner,
        site: Site.second)
      Flow.create!(name: "Artifact sort",
                  position: 3, organization: org, user: owner,
                  flow_steps_attributes: [{ step:, position: 1 }])
    end

    def create_patients(org)
      Patient.create!(case_number: "123", organization: org)
      patient = Patient.create!(case_number: "demo", organization: org)
      patient.inputs.attach(io:  File.open(Rails.root.join("test/fixtures/files/numbers.txt")),
      filename: "numbers1.txt",
      metadata: { file_type: DataFileType.first.data_type })
      patient.inputs.attach(io:  File.open(Rails.root.join("test/fixtures/files/numbers.txt")),
      filename: "numbers2.txt",
      metadata: { file_type: DataFileType.first.data_type })
      Patient.create!(case_number: "patient", organization: org)
    end

    def create_pipelines(org)
      flow = Flow.find_by(name: "Demo with number generation")

      patient_pipeline1 = Pipeline.create!(mode: :manual, name: "Pipeline 1", flow:, user: org.users.first, runnable: Patient.first)
      Computation.create!(user: org.users.first, step: flow.steps.first, pipeline: patient_pipeline1, status: :created)
      Computation.create!(user: org.users.first, step: flow.steps.second, pipeline: patient_pipeline1, status: :created)
      Computation.create!(user: org.users.first, step: flow.steps.third, pipeline: patient_pipeline1, status: :created)
      patient_pipeline1.outputs.attach(io:  File.open(Rails.root.join("test/fixtures/files/numbers.txt")),
      filename: "numbers.txt",
      metadata: { file_type: DataFileType.first.data_type })
      patient_pipeline1.outputs.attach(io:  File.open(Rails.root.join("test/fixtures/files/numbers.txt")),
      filename: "numbers2.txt",
      metadata: { file_type: DataFileType.first.data_type })

      patient_pipeline2 = Pipeline.create!(mode: :manual, name: "Pipeline 2", flow:, user: org.users.first, runnable: Patient.first)
      Computation.create!(user: org.users.first, step: flow.steps.first, pipeline: patient_pipeline2, script: "random script", status: :aborted, started_at: Time.current)
      Computation.create!(user: org.users.first, step: flow.steps.second, pipeline: patient_pipeline2, script: "random script", status: :error, started_at: Time.current)
      Computation.create!(user: org.users.first, step: flow.steps.third, pipeline: patient_pipeline2, status: :created)
      patient_pipeline2.outputs.attach(io:  File.open(Rails.root.join("test/fixtures/files/numbers.txt")),
      filename: "numbers.txt",
      metadata: { file_type: DataFileType.first.data_type })
      patient_pipeline2.outputs.attach(io:  File.open(Rails.root.join("test/fixtures/files/numbers.txt")),
      filename: "numbers2.txt",
      metadata: { file_type: DataFileType.first.data_type })


      org_pipeline = Pipeline.create!(mode: :manual, name: "Org Pipeline", flow:, user: org.users.first, runnable: org)
      Computation.create!(user: org.users.first, step: flow.steps.first, pipeline: org_pipeline, status: :created)
      Computation.create!(user: org.users.first, step: flow.steps.second, pipeline: org_pipeline, status: :created)
      Computation.create!(user: org.users.first, step: flow.steps.third, pipeline: org_pipeline, status: :created)
    end

    def create_artifacts(org)
      org.artifacts.attach(io:  File.open(Rails.root.join("test/fixtures/files/numbers.txt")),
      filename: "artifact1.txt")
      org.artifacts.attach(io:  File.open(Rails.root.join("test/fixtures/files/numbers.txt")),
      filename: "artifact2.txt")
    end

    def create_cohort(org)
      Cohort.create!(name: "Cohort", organization: org, patients: Patient.all)
    end
    def cpu_grant_type
      @cpu_grant_type ||= GrantType.find_or_create_by!(name: "cpu")
    end

    def gpu_grant_type
      @gpu_grant_type ||= GrantType.find_or_create_by!(name: "gpu")
    end
end
