# frozen_string_literal: true

class Patient::Uploader < ApplicationRecord
  belongs_to :cohort, optional: true
  belongs_to :organization
  enum status: {
    processing: 0,
    finished: 1,
    failed: 2
  }
  has_one_attached :patient_archive

  scope :recent, -> { where("updated_at > ?", 1.day.ago).order(updated_at: :desc) }

  validates_presence_of :status
  validates :patient_archive, presence: true, blob: { content_type: "application/zip" }


  after_commit :start, on: :create

  def start
    Patients::UploadJob.perform_later(self)
  end

  def recent?
    (Time.current - 1.day).before? updated_at
  end
end
