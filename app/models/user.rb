# frozen_string_literal: true

class User < ApplicationRecord
  serialize :proxy, Proxy

  include User::Account
  include User::Plgrid

  include User::Organizations
  include User::Pipelines
  include User::Flows
  include User::Steps

  def credentials_valid?
    proxy&.valid?
  end
end
