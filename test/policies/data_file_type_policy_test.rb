# frozen_string_literal: true

require "test_helper"

class DataFileTypePolicyTest < ActiveSupport::TestCase
  include PunditHelper

  test "Only admin can see data file types" do
    assert_permit(admin_pundit_user, DataFileType, :index)
    assert_no_permit(pundit_user, DataFileType, :index)
  end

  test "Only admin can manage organization data file types" do
    dft = data_file_types("image")

    assert_permit(admin_pundit_user, dft, :edit)
    assert_permit(admin_pundit_user, dft, :create)
    assert_permit(admin_pundit_user, dft, :update)
    assert_permit(admin_pundit_user, dft, :destroy)

    assert_no_permit(pundit_user, dft, :edit)
    assert_no_permit(pundit_user, dft, :create)
    assert_no_permit(pundit_user, dft, :update)
    assert_no_permit(pundit_user, dft, :destroy)
  end

  test "Admin cannot manage other organization data file types" do
    dft = create(:data_file_type, organization: organizations("other"))

    assert_no_permit(admin_pundit_user, dft, :edit)
    assert_no_permit(admin_pundit_user, dft, :create)
    assert_no_permit(admin_pundit_user, dft, :update)
    assert_no_permit(admin_pundit_user, dft, :destroy)
  end

  test "Admin can see only organization data file types" do
    create(:data_file_type, organization: organizations("other"))

    assert_equal data_file_types("image", "segmentation").sort,
                 DataFileTypePolicy::Scope.new(admin_pundit_user, DataFileType).resolve
  end
end
