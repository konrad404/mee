# frozen_string_literal: true

require "test_helper"

class Patients::UploaderTest < ActiveSupport::TestCase
  include ActionDispatch::TestProcess::FixtureFile
  setup do
    @uploader = create(:patient_uploader, patient_archive: fixture_file_upload("patients.zip"))
  end


  test "it uploads the patient file" do
    assert_changes "Patient.count", 4 do
      assert_changes "ActiveStorage::Attachment.count", 4 do
        Patients::Uploader.new(@uploader).call
      end
    end
    assert @uploader.finished?
  end

  test "it assigns patients to cohort" do
    cohort = create(:cohort, name: "unique", patients: [])
    @uploader.update(cohort:)

    assert_changes "cohort.patients.count", 4 do
      Patients::Uploader.new(@uploader).call
    end
  end

  test "it fails with duplicate patient name" do
    create(:patient, case_number: "1")
    Patients::Uploader.new(@uploader).call
    assert_equal "Patients with case numbers 1 already exist.", @uploader.error_message

    assert @uploader.failed?
  end

  test "it fails for nested directories" do
    @uploader.patient_archive.attach fixture_file_upload("patients_nested.zip")
    Patients::Uploader.new(@uploader).call
    assert_equal "Nested directories detected", @uploader.error_message

    assert @uploader.failed?
  end
end
