# frozen_string_literal: true

require "test_helper"

class GuardedExecutorTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper

  test "invoke block when credentials are valid" do
    invoked = false
    GuardedExecutor.new(user_with_valid_credentials).call do
      invoked = true
    end

    assert invoked, "Block should be invoked"
  end

  test "don't notify when credentials are valid" do
    assert_no_changes "ActionMailer::Base.deliveries.count" do
      GuardedExecutor.new(user_with_valid_credentials).call
    end
  end

  test "don't invoke block when credentials are not valid" do
    GuardedExecutor.new(user_with_invalid_credentials).call do
      flunk "Block should not be invoked"
    end
  end

  test "Notify user via email about invalid/outdated credentials" do
    perform_enqueued_jobs do
      assert_changes "ActionMailer::Base.deliveries.count" do
        GuardedExecutor.new(user_with_invalid_credentials).call
      end
    end
  end

  test "sends 1 email every day about credentials expiration" do
    assert_changes "ActionMailer::Base.deliveries.count", 2 do
      perform_enqueued_jobs do
        GuardedExecutor.new(user_with_invalid_credentials).call
        GuardedExecutor.new(user_with_invalid_credentials).call

        travel 25.hours
        GuardedExecutor.new(user_with_invalid_credentials).call
      end
    end
  end

  private
    def user_with_valid_credentials
      users("user").tap { |u| u.stubs(:credentials_valid?).returns(true) }
    end

    def user_with_invalid_credentials
      users("user").tap { |u| u.stubs(:credentials_valid?).returns(false) }
    end
end
