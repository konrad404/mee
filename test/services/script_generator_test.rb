# frozen_string_literal: true

require "test_helper"

class ScriptGeneratorTest < ActiveSupport::TestCase
  include GitlabHelper
  include ActionDispatch::TestProcess::FixtureFile
  include LinkHelper
  include DataverseTestHelper

  test "add error when active grant cannot be found" do
    computation = build(:computation)
    errors = ActiveModel::Errors.new(computation)

    travel_to(1.year.from_now) do
      ScriptGenerator.new(computation, "{{ grant_id }}", errors).call
    end


    assert_includes errors[:script], "active grant cannot be found"
  end

  test "adds error when input file is not found" do
    computation = build(:computation)
    create(:data_file_type, data_type: "provenance")
    errors = ActiveModel::Errors.new(computation)

    ScriptGenerator.new(computation, "{% stage_in provenance %}", errors).call

    assert_includes errors[:script],
                   "cannot find provenance data file in patient or pipeline directories"
  end

  test "inserts upload file to dataverse dataset curl" do
    computation, dataverse_url, token = dataverse_test_setup

    upload_script = ScriptGenerator.new(
      computation,
      "{% dataverse_file_stage_out hello.txt dataset_doi %}"
    ).call

    url = "#{dataverse_url}/api/datasets/:persistentId/add?persistentId=dataset_doi"
    filename = "$SCRATCHDIR/$(basename #{url})"

    assert_includes upload_script, "curl -w \"%{http_code}\" -o #{filename} -H \"X-Dataverse-key:#{token}\" -X POST -F file=@hello.txt #{url}"
  end

  test "inserts upload file with metadata to dataverse dataset curl" do
    computation, dataverse_url, token = dataverse_test_setup

    upload_script = ScriptGenerator.new(
      computation,
      "{% dataverse_file_stage_out hello.txt dataset_doi {\"directoryLabel\":\"dir1/subdir\"} %}"
    ).call

    url = "#{dataverse_url}/api/datasets/:persistentId/add?persistentId=dataset_doi"
    filename = "$SCRATCHDIR/$(basename #{url})"

    assert_includes upload_script, "curl -w \"%{http_code}\" -o #{filename} -H \"X-Dataverse-key:#{token}\" -X POST -F file=@hello.txt -F jsonData='{\"directoryLabel\":\"dir1/subdir\"} ' #{url}"
  end

  test "inserts download dataset from dataverse curl" do
    computation, dataverse_url, token = dataverse_test_setup

    download_script = ScriptGenerator.new(
      computation,
      "{% dataverse_dataset_stage_in dataset_doi %}"
    ).call

    url = "#{dataverse_url}/api/access/dataset/:persistentId/?persistentId=dataset_doi"
    filename = "$SCRATCHDIR/$(basename #{url})"

    assert_includes download_script, "curl -L -w \"%{http_code}\" -o #{filename} -H \"X-Dataverse-key:#{token}\" #{url}"
  end

  test "inserts download dataset as dataset.zip from dataverse curl" do
    computation, dataverse_url, token = dataverse_test_setup

    download_script = ScriptGenerator.new(
      computation,
      "{% dataverse_dataset_stage_in dataset_doi dataset %}"
    ).call

    url = "#{dataverse_url}/api/access/dataset/:persistentId/?persistentId=dataset_doi"

    assert_includes download_script, "curl -L -w \"%{http_code}\" -o $SCRATCHDIR/dataset -H \"X-Dataverse-key:#{token}\" #{url}"
  end

  test "inserts download file from dataverse curl" do
    computation, dataverse_url, token = dataverse_test_setup

    download_script = ScriptGenerator.new(
      computation,
      "{% dataverse_file_stage_in file_doi %}"
    ).call

    url = "#{dataverse_url}/api/access/datafile/:persistentId/?persistentId=file_doi"

    assert_includes download_script, "curl -L -O -J -w \"%{http_code}\" -H \"X-Dataverse-key:#{token}\" #{url}"
  end

  test "inserts download file as filename from dataverse curl" do
    computation, dataverse_url, token = dataverse_test_setup

    download_script = ScriptGenerator.new(
      computation,
      "{% dataverse_file_stage_in file_doi filename %}"
    ).call

    url = "#{dataverse_url}/api/access/datafile/:persistentId/?persistentId=file_doi"

    assert_includes download_script, "curl -L -o filename -J -w \"%{http_code}\" -H \"X-Dataverse-key:#{token}\" #{url}"
  end

  test "adds error when dataverse instance is not found" do
    computation = build(:computation)
    errors = ActiveModel::Errors.new(computation)

    organizations("main").update(dataverse_url: "")

    ScriptGenerator.new(computation, "{% dataverse_file_stage_out hello.txt dataset_doi %}", errors).call
    assert_includes errors[:script],
                    "This step requires Dataverse configuration. Please ask organization's administrator for Dataverse support."
  end

  test "adds error when dataverse token is not found" do
    computation = build(:computation)
    errors = ActiveModel::Errors.new(computation)

    memberships("user").update(dataverse_token: "")

    ScriptGenerator.new(computation, "{% dataverse_file_stage_out hello.txt dataset_doi %}", errors).call
    assert_includes errors[:script],
                    "This step requires Dataverse configuration. Dataverse token is missing in your profile!"
  end

  test "inserts upload file for file type" do
    patient = build(:patient)
    pipeline = create(:pipeline, runnable: patient)
    computation = create(:computation, pipeline:, status: :running, secret: SecureRandom.base58)
    dft = create(:data_file_type,
                  name: "TestDataFileType",
                  viewer: :text,
                  data_type: "test_data_file_type", pattern: /^test_data_file.*\.txt$/)
    computation.step.required_file_types << dft
    pipeline.inputs.attach(io: fixture_file_upload("outdated_proxy"),
      filename: "test_data_file1.txt",
      metadata: { file_type: dft.data_type })

    script = ScriptGenerator.new(
      computation,
      "{% stage_in #{dft.data_type} %}"
    ).call
    assert_includes script, computation_type_inputs_url(computation, secret: computation.secret,
      type: dft.data_type, script_name: script_name(computation))
  end

  test "inserts upload file for filename" do
    patient = build(:patient)
    pipeline = create(:pipeline, runnable: patient)
    computation = create(:computation, pipeline:, status: :running, secret: SecureRandom.base58)
    pipeline.inputs.attach(io: fixture_file_upload("outdated_proxy"),
      filename: "test_data_file1.txt")

    script = ScriptGenerator.new(
      computation,
      "{% stage_in_by_filename test_data_file1.txt %}"
    ).call
    assert_includes script, computation_filename_inputs_url(computation, secret: computation.secret,
      filename: "test_data_file1.txt", script_name: script_name(computation))
  end

  test "inserts download file curl" do
    computation = create(:computation, secret: SecureRandom.base58)
    script = ScriptGenerator.new(computation,
                                 "{% stage_out foo.txt %}").call

    assert_includes script, computation_outputs_url(computation, secret: computation.secret,
      script_name: script_name(computation))
  end

  test "inserts download artifact curl" do
    computation = create(:computation, secret: SecureRandom.base58)
    script = ScriptGenerator.new(computation,
                                 "{% stage_in_artifact $outdated_proxy %}").call

    assert_includes script, "http://mee.lvh.me:3000/1059638630/computations/#{computation.id}/artifacts/#{computation.secret}/$outdated_proxy"
  end

  test "inserts upload artifact curl new" do
    computation = create(:computation, secret: SecureRandom.base58)
    script = ScriptGenerator.new(computation,
                                 "{% stage_out_artifact outdated_proxy} %}").call

    assert_includes script, computation_upload_artifact_url(computation, secret: computation.secret, script_name: script_name(computation))
  end

  test "raises error for wrong" do
    computation = create(:computation, secret: SecureRandom.base58)
    errors = computation.errors
    ScriptGenerator.new(computation, "{% wrong_tag foo.txt %}", errors).call

    assert_includes errors[:script], "Liquid syntax error: Unknown tag 'wrong_tag'"
  end

  test "inserts repository sha to clone" do
    script = ScriptGenerator.new(create(:computation, revision: "rev"),
                                 "{{ revision }}").call

    assert_includes script, "rev"
  end

  test "inserts clone repo command" do
    organization = organizations("main")
    computation = build(:computation, revision: "rev")
    script = ScriptGenerator.new(computation, "{% clone_repo %}").call

    assert_includes script, "export SSH_DOWNLOAD_KEY=\"#{organization.git_config.download_key}"
    assert_includes script, "git clone git@#{organization.git_config.host}:#{computation.repository}"
    assert_match(/cd.*repo.*/, script)
    assert_includes script, "git reset --hard rev"
  end

  test "inserts license_for ansys script" do
    create(:license,
          name: "ansys",
          config: [License::Entry.new(key: "ansysli_servers",
                                      value: "ansys-servers"),
                    License::Entry.new(key: "ansyslmd_license_file",
                                      value: "ansys-license-file")
                    ])

    script = ScriptGenerator.new(create(:computation, revision: "rev"),
                                "{% license_for ansys %}").call

    assert_includes script, "export ANSYSLI_SERVERS=ansys-servers"
    assert_includes script, "export ANSYSLMD_LICENSE_FILE=ansys-license-file"
  end

  test "adds error when requested license is not found" do
    computation = build(:computation)
    errors = ActiveModel::Errors.new(computation)

    ScriptGenerator.new(computation, "{% license_for ansys %}", errors).call

    assert_includes errors[:script], "cannot find requested ansys license"
  end


  test "inserts value_of tag-or-branch and grant" do
    step = steps("first")
    stub_repo_versions(step,
                      { branches: ["main"], tags: ["t1"] })

    computation = create(:computation,
                         step:,
                          parameter_values_attributes: {
                            "tag-or-branch" => { "version" => "main" },
                            "grant" => { "value" => grants("cpu").name }
                          })

    script = ScriptGenerator.new(computation,
                                "{% value_of tag-or-branch %}").call

    assert_includes "main", script
  end

  test "adds error when requested value is not found" do
    computation = build(:computation)
    errors = ActiveModel::Errors.new(computation)

    ScriptGenerator.new(computation, "{% value_of tag-or-branch %}", errors).call

    assert_includes errors[:script], "parameter value tag-or-branch was not found"
  end

  test "inserts pipeline identifier" do
    patient = build(:patient, case_number: "Case 312")
    pipeline = build(:pipeline, runnable: patient)
    computation = create(:computation, pipeline:)

    script = ScriptGenerator.new(computation, "{{ pipeline_identifier }}").call

    assert_equal "case-312-#{pipeline.iid}", script
  end

  test "inserts pipeline name" do
    patient = build(:patient, case_number: "Case 312")
    pipeline = build(:pipeline, runnable: patient)
    computation = create(:computation, pipeline:)

    script = ScriptGenerator.new(computation, "{{ pipeline_name }}").call

    assert_equal "#{pipeline.name}", script
  end

  test "inserts step name" do
    patient = build(:patient, case_number: "Case 312")
    pipeline = build(:pipeline, runnable: patient)
    computation = create(:computation, pipeline:)

    script = ScriptGenerator.new(computation, "{{ step_name }}").call

    assert_equal "#{computation.name}", script
  end

  test "inserts patient case_number" do
    patient = build(:patient, case_number: "Case 312")
    pipeline = build(:pipeline, runnable: patient)
    computation = build(:computation, pipeline:)

    script = ScriptGenerator.new(computation, "{{ case_number }}").call

    assert_equal "Case 312", script
  end

  test "inserts user email" do
    user = users("user")
    pipeline = build(:pipeline, user:)
    computation = build(:computation, pipeline:)

    script = ScriptGenerator.new(computation, "{{ email }}").call

    assert_equal script, user.email
  end

  test "inserts pipeline mode" do
    pipeline = build(:pipeline, mode: "automatic")
    computation = build(:computation, pipeline:)

    script = ScriptGenerator.new(computation, "{{ mode }}").call

    assert_equal script, "automatic"
  end

  test "inserts user proxy certificate" do
    user = users("user")
    computation = build(:computation, user:)

    script = ScriptGenerator.new(computation, "{{ proxy }}").call
    assert_includes script, user.proxy.to_s
  end
end
