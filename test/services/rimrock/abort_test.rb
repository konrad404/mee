# frozen_string_literal: true

require "test_helper"

class Rimrock::AbortTest < ActiveSupport::TestCase
  include RimrockHelper

  setup do
    @updater = stub(new: stub(call: true))
  end

  test "aborts active computation and notify about this event" do
    computation = create_computation(status: :running)
    stub_job_abort(computation)

    updater_instance = mock
    updater_instance.expects(:call)
    @updater.expects(:new).returns(updater_instance)

    assert_changes "computation.status", to: "aborted" do
      call(computation)
    end
  end

  test "does nothing for non running computations" do
    computation = create_computation(status: :finished)
    stub_job_abort(computation)

    assert_no_changes "computation.status" do
      call(computation)
    end
  end

  test "only updates computation status to aborted when proxy is outdated" do
    computation = create_computation(status: :running)

    travel 2.days do
      call(computation)
    end

    assert_equal "aborted", computation.status
  end

  private
    def create_computation(status:)
      create(:computation, user: users("user"), status:, job_id: "jid", started_at: Time.current)
    end

    def call(computation)
      Rimrock::Abort.new(computation, @updater).call
    end
end
