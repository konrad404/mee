# frozen_string_literal: true

require "test_helper"

class Computation::BroadcasterTest < ActiveSupport::TestCase
  include ActionCable::TestHelper

  test "broadcasts status and show after status update" do
    computation = create(:computation, started_at: Time.current)

    assert_broadcasts(dom_id(computation.pipeline, "show"), 2) do
      computation.finished!
    end
  end

  test "does not broadcast if status is not updated" do
    pipeline = create(:pipeline, campaign: create(:campaign))
    computation = create(:computation, started_at: Time.current, pipeline:)

    assert_no_broadcasts(dom_id(computation.pipeline, "show")) do
      computation.update(script: "new script")
    end
  end

  test "does not broadcast if pipeline belongs to campaign" do
    pipeline = create(:pipeline, campaign: create(:campaign))
    computation = create(:computation, started_at: Time.current, pipeline:)

    assert_no_broadcasts(dom_id(computation.pipeline, "show")) do
      computation.finished!
    end
  end
end
