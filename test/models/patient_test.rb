# frozen_string_literal: true

require "test_helper"

class PatientTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper
  include ActionDispatch::TestProcess::FixtureFile

  ["", "new"].each do |case_number|
    test "'#{case_number}' should not be a valid case number" do
      assert_not build(:patient, case_number:).valid?
    end
  end

  test "#status returns last pipeline status" do
    patient = create(:patient)
    create(:pipeline, runnable: patient, status: :error)
    create(:pipeline, runnable:  patient, status: :success)

    assert_equal "success", patient.status
  end

  test "attaching a file for patient queues Pipelines::StartRunnableJob" do
    Current.organization = organizations("main")
    patient = create(:patient)
    create(:pipeline, runnable: patient)
    create(:pipeline, runnable: patient)
    assert_enqueued_jobs(2, only: Pipelines::StartRunnableJob,) do
      patient.add_input(fixture_file_upload("outdated_proxy"))
    end
  end

  test "picks oldest file when there are two of the same type" do
    patient = create(:patient)
    create(:data_file_type, data_type: "type")
    patient.inputs.attach(io: fixture_file_upload("outdated_proxy"), filename: "input1", metadata: { file_type: "type" })
    patient.inputs.attach(io: fixture_file_upload("outdated_proxy"), filename: "input2", metadata: { file_type: "type" })

    assert_equal patient.pick_file_by_type("type"), patient.inputs_blobs.first
  end

  test "picks file by name" do
    patient = create(:patient)
    patient.inputs.attach(io: fixture_file_upload("outdated_proxy"), filename: "input1", metadata: { file_type: "type" })
    patient.inputs.attach(io: fixture_file_upload("outdated_proxy"), filename: "input2", metadata: { file_type: "type" })

    assert_equal patient.pick_file_by_filename("input1"), patient.inputs_blobs.first
  end
end
