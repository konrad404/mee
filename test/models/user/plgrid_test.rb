# frozen_string_literal: true

require "test_helper"

class User::PlgridTest < ActiveSupport::TestCase
  include CertHelper

  test "plgrid login creates new user if does not exist" do
    assert_difference "User.count", 1, "Expected a new user to be created" do
      User.from_plgrid_omniauth(auth("plgnewuser", proxy: outdated_proxy)).save
    end
  end

  test "plgrid login uses auth info to populate user data" do
    proxy = outdated_proxy
    user = User.from_plgrid_omniauth(auth("plgnewuser", proxy:))

    assert_equal "plgnewuser", user.plgrid_login
    assert_equal "plgnewuser@b.c", user.email
    assert_equal "plgnewuser", user.first_name
    assert_equal "Last Name", user.last_name
    assert_equal proxy, user.proxy
  end

  test "nil proxy when plgrid login does not return simple CA data" do
    user = User.from_plgrid_omniauth(auth("plgnewuser", proxy: nil))

    assert_equal Proxy.for(nil), user.proxy
  end

  test "plgrid login creates organization membership" do
    org = organizations("main")
    user = User.from_plgrid_omniauth(
      auth("plgnewuser", proxy: outdated_proxy, teams: [org.plgrid_team_id]))

    assert_equal org, user.memberships.first.organization
  end

  private
    def auth(plglogin, proxy:, teams: [])
      OpenStruct.new(info: OpenStruct.new(
        nickname: plglogin,
        email: "#{plglogin}@b.c",
        name: "#{plglogin} Last Name",
        proxy: proxy&.proxy_cert,
        proxyPrivKey: proxy&.proxy_priv_key,
        userCert: proxy&.user_cert,
        userteams: teams.map { |t| "#{t}(#{t})" }.join(",")
      ))
    end
end
