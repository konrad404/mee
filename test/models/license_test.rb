# frozen_string_literal: true

require "test_helper"

class LicenseTest < ActiveSupport::TestCase
  test "lifetime interval validation" do
    assert build(:license, starts_at: Time.zone.today, ends_at: 1.day.from_now).valid?
    assert_not build(:license, starts_at: 1.day.from_now, ends_at: Time.zone.today).valid?
  end

  test "#to_script which exports license entries" do
    license = build(:license,
                    config: [
                      License::Entry.new(key: "aa_bb", value: "http://a.b.pl"),
                      License::Entry.new(key: "other", value: "other value")
                    ])

    script = license.to_script

    assert_includes script, "export AA_BB=http://a.b.pl"
    assert_includes script, "export OTHER=other value"
  end
end
