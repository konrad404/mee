# frozen_string_literal: true

require "test_helper"

class Campaign::StatusCalculatorTest < ActiveSupport::TestCase
  test "should be created when pipelines are ready" do
    campaign =  create(:campaign, status: :initializing,
                pipelines: create_list(:pipeline, 2))

    campaign.update(desired_pipelines: 2)

    assert_equal :created, Campaign::StatusCalculator.new(campaign).calculate
  end

  test "Should be failed when all pipelines creation fail" do
    campaign = create(:campaign, failed_pipelines: 2)
    campaign.update(desired_pipelines: 2)

    assert_equal :creating_pipelines_failed, Campaign::StatusCalculator.new(campaign).calculate
  end

  test "should be running when any pipeline is running" do
    campaign =  create(:campaign, status: :created,
                pipelines: [create(:pipeline, status: :running),
                            create(:pipeline)])

    assert_equal :running, Campaign::StatusCalculator.new(campaign).calculate
  end

  test "should be finished when all pipelines finish" do
    cohort = create(:cohort, patients: build_list(:patient, 2))
    campaign =  create(:campaign, cohort:, status: :running,
                pipelines: create_list(:pipeline, 2, status: :success))

    assert_equal :finished, Campaign::StatusCalculator.new(campaign).calculate
  end
end
