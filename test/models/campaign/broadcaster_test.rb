# frozen_string_literal: true

require "test_helper"

class Campaign::BroadcasterTest < ActiveSupport::TestCase
  include ActionCable::TestHelper

  test "broadcasts progress bar, run button and status after status update" do
    campaign = create(:campaign, status: :initializing)

    # 2 from after_save, 2 from after_update
    assert_broadcasts(campaign.to_gid_param, 4) do
      campaign.finished!
    end
  end

  test "does not update progress bar after name update" do
    campaign = create(:campaign, status: :initializing)

    # 2 from after_update
    assert_broadcasts(campaign.to_gid_param, 2) do
      campaign.update(name: "new_name")
    end
  end
end
