# frozen_string_literal: true

require "test_helper"

class Parameter::SelectTest < ActiveSupport::TestCase
  setup do
    @parameter = build(:select_parameter)
  end

  test "#values fields has at least 2 options" do
    @parameter.values = []
    assert_not @parameter.valid?
    assert_includes @parameter.errors[:values], "Please provide at least two options"

    @parameter.values = [Parameter::Select::Value.new(name: "A", value: "a")]
    assert_not @parameter.valid?
    assert_includes @parameter.errors[:values], "Please provide at least two options"

    @parameter.values = [
      Parameter::Select::Value.new(name: "A", value: "a"),
      Parameter::Select::Value.new(name: "B", value: "b")
    ]
    assert @parameter.valid?
  end

  test "has unique values" do
    @parameter.values = [
      Parameter::Select::Value.new(name: "A", value: "a "),
      Parameter::Select::Value.new(name: "AA", value: "a")
    ]

    assert_not @parameter.valid?
    assert_not_empty @parameter.values.first.errors[:value]
    assert_not_empty @parameter.values.second.errors[:value]
  end

  test "has unique names" do
    @parameter.values = [
      Parameter::Select::Value.new(name: "A", value: "a "),
      Parameter::Select::Value.new(name: " A ", value: "aa")
    ]

    assert_not @parameter.valid?
    assert_not_empty @parameter.values.first.errors[:name]
    assert_not_empty @parameter.values.second.errors[:name]
  end

  test "validates default value" do
    @parameter.values = [
      Parameter::Select::Value.new(name: "A", value: "a"),
      Parameter::Select::Value.new(name: "B", value: "b")
    ]

    @parameter.default_value = nil
    assert @parameter.valid?

    @parameter.default_value = ""
    assert @parameter.valid?

    @parameter.default_value = "a"
    assert @parameter.valid?

    @parameter.default_value = "c"
    assert_not @parameter.valid?
    assert_not_empty @parameter.errors[:default_value]
  end
end
