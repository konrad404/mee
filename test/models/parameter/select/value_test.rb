# frozen_string_literal: true

require "test_helper"

class Parameter::Select::ValueTest < ActiveSupport::TestCase
  test "strips label" do
    select_value = Parameter::Select::Value.new(name: "  A b c  ")

    assert_equal "A b c", select_value.name
  end

  test "strips value" do
    select_value = Parameter::Select::Value.new(value: "  a b c  ")

    assert_equal "a b c", select_value.value
  end
end
