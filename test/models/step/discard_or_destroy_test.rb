# frozen_string_literal: true

require "test_helper"

class Step::DiscardOrDestroyTest < ActiveSupport::TestCase
  test "step is deleted when no depending flows" do
    step = create(:step)

    assert_difference "Step.count", -1, "Step should be deleted" do
      step.discard_or_destroy
    end
  end

  test "can't discard when pipeline is using the step" do
    flow = flows("first")
    step = flow.steps.first
    create(:pipeline, flow:)


    assert_no_difference "Step.count" do
      step.discard_or_destroy
    end
    assert_not step.discarded?
  end

  test "can discard step for discarded flow" do
    flow = flows("first")
    step = flow.steps.first

    create(:pipeline, flow:)
    flow.update(discarded_at: Time.current)

    assert_no_difference "Step.count" do
      step.discard_or_destroy
    end
    assert step.discarded?
  end
end
