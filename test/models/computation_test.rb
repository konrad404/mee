# frozen_string_literal: true

require "test_helper"

class ComputationTest < ActiveSupport::TestCase
  test "#active returns only script_generated, queued or running computations" do
    computation = create(:computation, status: "script_generated")
    assert_equal [computation], Computation.active

    computation.update(started_at: Time.current, status: "queued")
    assert_equal [computation], Computation.active

    computation.update(status: "running")
    assert_equal [computation], Computation.active

    computation.update(status: "finished")
    assert_equal [], Computation.active
  end

  test "#submitted returns only queued and running computations" do
    create(:computation, status: "script_generated")
    queued = create(:computation, status: "queued")
    running = create(:computation, status: "running")

    assert_equal [queued, running].sort, Computation.submitted.sort
  end

  test "Computation is configured only if tag or branch is set" do
    assert_not build(:computation).configured?
    assert build(:computation, parameter_values_attributes: { tag_or_branch: { value: "master" } }).configured?
  end

  test "runnable? returns correct values with wait for previous set to true" do
    flow = create(:flow)
    step1 = create(:step)
    step2 = create(:step)
    create(:flow_step, flow:, step: step1)
    create(:flow_step, flow:, step: step2, wait_for_previous: true)
    pipeline = create(:pipeline, flow:)
    computation1 = create(:computation, pipeline:, step: step1, status: :running, started_at: Time.now)
    computation2 = create(:computation, pipeline:, step: step2, status: :created)

    assert_equal computation2.runnable?, false
    computation1.update(status: :finished)
    assert_equal computation2.runnable?, true
  end
end
