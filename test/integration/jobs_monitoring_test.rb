# frozen_string_literal: true

require "test_helper"

class JobsMonitoringTest < ActionDispatch::IntegrationTest
  setup do
    in_root!
  end

  test "admin sees jobs monitoring UI" do
    login_as users("admin")
    get admin_jobs_path

    assert_match I18n.t("layouts.root.navbar.jobs"), response.body
  end

  test "normal user cannot see jobs monitoring UI" do
    login_as users("user")

    assert_raise ActionController::RoutingError do
      get admin_jobs_path
    end
  end
end
