# frozen_string_literal: true

require "test_helper"
require "minitest/autorun"

class TriggerUpdateJobTest < ActiveJob::TestCase
  test "triggers update for all users with active jobs" do
    u1, u2 = create_list(:user, 2)

    create(:computation, status: "running", user: u1)
    create(:computation, status: "finished", user: u2)

    mock = MiniTest::Mock.new
    mock.expect(:call, true, [u1])

    Rimrock::UpdateJob.stub(:perform_later, mock) do
      TriggerUpdateJob.perform_now
    end
  end
end
