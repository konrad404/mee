# frozen_string_literal: true

require "test_helper"
require "minitest/autorun"

class Rimrock::UpdateJobTest < ActiveJob::TestCase
  test "triggers user computations update" do
    update = MiniTest::Mock.new
    update.expect(:call, true)

    mock = MiniTest::Mock.new
    mock.expect(:call, update, [users("user")],
                on_finish_callback: PipelineUpdater)

    Rimrock::Update.stub(:new, mock) do
      Rimrock::UpdateJob.perform_now(users("user"))
    end
  end
end
