# frozen_string_literal: true

require "test_helper"
require "minitest/autorun"

class Rimrock::StartJobTest < ActiveJob::TestCase
  test "triggers user computations update" do
    computation = create(:computation, started_at: Time.current)
    updated_at = computation.updated_at

    start = MiniTest::Mock.new
    start.expect(:call, true)

    Rimrock::Start.stub(:new, start) do
      Rimrock::StartJob.perform_now(computation)
    end

    assert_not_equal updated_at, computation.updated_at
  end

  # todo refactor this test with pipeline broadcaster
  test "triggers computation update after error" do
    computation = create(:computation, started_at: Time.current)
    updated_at = computation.updated_at

    start = Object.new
    def start.call = raise

    Rimrock::Start.stub(:new, start) do
      Rimrock::StartJob.perform_now(computation)
    end

    assert_not_equal updated_at, computation.updated_at
  end
end
