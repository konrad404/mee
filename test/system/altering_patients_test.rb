# frozen_string_literal: true

require "test_helper"
require "application_system_test_case"
class AlteringPatientsTest < ApplicationSystemTestCase
  def setup
    in_organization! organizations("main")
    login_as users("user")
  end

  test "lets the user register a case with case number" do
    visit new_patient_path

    assert_text I18n.t "simple_form.labels.patient.case_number"

    fill_in "patient[case_number]", with: "888"

    assert_changes "Patient.count" do
      click_button I18n.t("register")
      assert_text I18n.t "patients.create.success"
      assert_current_path patient_path(Patient.first)
    end
  end

  test "blocks taken case number registration" do
    case_number = create(:patient).case_number
    visit new_patient_path

    fill_in "patient[case_number]", with: case_number

    assert_no_changes "Patient.count" do
      click_button I18n.t("register")
      assert_text "Case number has already been taken"
    end
  end

  test "lets the user register a case with uncommon characters" do
    visit new_patient_path

    fill_in "patient[case_number]", with: "-_."

    click_button I18n.t("register")
    assert_text "-_."
    assert_current_path patient_path(Patient.first)
  end

  test "allows to cancel the case registration" do
    visit new_patient_path

    click_link I18n.t("cancel")

    assert_current_path patients_path
  end

  test "makes it possible to remove a chosen case" do
    patient = create(:patient)
    visit patient_path(patient)

    assert_changes "Patient.count", -1 do
      click_on I18n.t("patients.show.remove")
      click_button "Delete", match: :first
      assert_text I18n.t("patients.destroy.success", case_number: patient.case_number)
    end
  end
end
