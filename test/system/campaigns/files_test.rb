# frozen_string_literal: true

require "application_system_test_case"

class Campaigns::FilesTest < ApplicationSystemTestCase
  include ActionDispatch::TestProcess::FixtureFile

  setup do
    in_organization! organizations("main")
    login_as users("admin")

    campaign = create :campaign
    pipeline = create(:pipeline, campaign:)
    pipeline.outputs.attach(fixture_file_upload("outdated_proxy"))
    @data_file = pipeline.outputs.first

    visit campaign_path(campaign)
  end

  test "Campaign files are collapsed by default" do
    assert_no_text @data_file.blob.filename
  end

  test "Can see campaign files after expanding files tab" do
    click_on I18n.t("campaigns.view.output_files.title")
    assert_text I18n.t("campaigns.view.output_files.patient.title", case_number:
      Pipeline.first.runnable.case_number)
    assert_text @data_file.blob.filename
  end
end
