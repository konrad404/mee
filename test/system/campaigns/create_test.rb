# frozen_string_literal: true

require "application_system_test_case"
require "minitest/autorun"

class Campaigns::CreateTest < ApplicationSystemTestCase
  include RimrockHelper
  def setup
    stub_rimrock_process
    create(:cohort, patients: build_list(:patient, 3))
    in_organization! organizations("main")
    login_as users("admin")
    visit new_campaign_path
    fill_in "Name", with: "CampaignName"
  end

  test "creates a campaign with correct arguments" do
    Campaigns::CreatePipelines.any_instance.stubs(:call)
    click_on I18n.t("campaigns.create.submit")
    assert_text I18n.t("campaigns.create.success", name: "CampaignName")
    assert_equal current_path, campaign_path(Campaign.first)
  end

  test "fails to create campaign with incorrect arguments" do
    first("#pipeline_parameters_values_#{Flow.first.steps.first.slug}_grant_value").select ""
    click_on I18n.t("campaigns.create.submit")
    assert_text I18n.t("campaigns.errors.steps_attributes")
    assert_equal 0, Campaign.count
  end
end
