# frozen_string_literal: true

require "test_helper"
require "application_system_test_case"
module Artifacts
  class EditTest < ApplicationSystemTestCase
    include ActionDispatch::TestProcess::FixtureFile

    def setup
      @organization = organizations("main")
      in_organization! @organization
      login_as users("user")
      @organization.artifacts.attach(io: fixture_file_upload("outdated_proxy"), filename: "output")
      @artifact = @organization.artifacts.first
    end

    test "it updates artifact and shows notification" do
      visit artifacts_path
      find_link(href: edit_artifact_path(@artifact)).click
      find("#artifact_filename").set("Updated name")
      click_on("Update Artifact")

      assert_text "Artifact successfully updated"
      assert_text "Updated name"
    end

    test "doesn't allow blank name" do
      visit artifacts_path
      find_link(href: edit_artifact_path(@artifact)).click
      find("#artifact_filename").set("")
      click_on("Update Artifact")

      assert_text "Filename can't be blank"
    end

    test "doesn't allow taken name" do
      @organization.artifacts.attach(io: fixture_file_upload("outdated_proxy"), filename: "taken.txt")
      visit artifacts_path
      find_link(href: edit_artifact_path(@artifact)).click
      find("#artifact_filename").set("taken.txt")
      click_on("Update Artifact")

      assert_text "Filename has already been taken"
    end
  end
end
