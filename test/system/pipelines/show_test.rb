# frozen_string_literal: true

require "application_system_test_case"

class Pipelines::ShowTest < ApplicationSystemTestCase
  include ActionDispatch::TestProcess::FixtureFile

  def setup
    in_organization! organizations("main")
    login_as users("user")
    @pipeline = create(:pipeline, runnable: create(:patient), name: "p1")
  end

  test "has link for patient index and patient show" do
    visit pipeline_path(@pipeline)

    assert_link href: patients_path
    assert_link href: patient_path(@pipeline.runnable)
  end

  test "displays basic info about a pipeline" do
    visit pipeline_path(@pipeline)

    assert_text @pipeline.name
    assert_text @pipeline.flow.name
    assert_text @pipeline.user.name
  end

  test "shows alert when no computation defined" do
    visit pipeline_path(@pipeline)
    assert_text I18n.t("pipelines.show.no_computations")
  end

  test "redirects into first defined computation" do
    computation = create(:computation, pipeline: @pipeline)
    visit pipeline_path(@pipeline)

    assert_current_path computation_path(computation)
  end

  test "lists pipeline computations in correct flow order" do
    computation1 = create(:computation, pipeline: @pipeline)
    computation2 = create(:computation, pipeline: @pipeline)
    computation3 = create(:computation, pipeline: @pipeline)
    visit pipeline_path(@pipeline)

    assert_selector ".pipeline li:nth-child(1) a", text: computation1.name
    assert_selector ".pipeline li:nth-child(2) a", text: computation2.name
    assert_selector ".pipeline li:nth-child(3) a", text: computation3.name
  end

  test "deletes the data file as owner" do
    @pipeline.inputs.attach(fixture_file_upload("outdated_proxy"))
    data_file = @pipeline.inputs.first
    visit pipeline_path(@pipeline)

    click_link "Delete"
    accept_modal do
      click_button "Delete"
    end
    assert_no_text data_file.blob.filename
  end

  test "doesn't see delete button as non owner" do
    login_as users("stranger")
    @pipeline.inputs.attach(fixture_file_upload("outdated_proxy"))
    visit pipeline_path(@pipeline)

    assert_no_text "Delete"
  end
end
