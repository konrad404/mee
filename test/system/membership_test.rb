# frozen_string_literal: true

require "minitest/autorun"
require "application_system_test_case"

class MembershipTest < ApplicationSystemTestCase
  test "Blocks content for non-approved users" do
    in_organization! organizations("main")
    login_as(users("user"), teams: "")

    assert_content "no access"
  end

  test "Shows content for approved users" do
    in_organization! organizations("main")
    login_as(users("user"))

    assert_content users("user").name
  end
end
