# left for later implementation, because requires net/dav which is temporarily removed

=begin
# frozen_string_literal: true

require "rails_helper"

RSpec.describe "Comparing two pipelines", files: true do
  let(:patient) { create(:patient, :with_pipeline) }
  let!(:pipelines) { create_list(:pipeline, 2, runnable: patient) }
  let(:user) { create(:user) }

  before(:each) do
    create(:data_file_type,
           name: "Blood flow model",
           data_type: "blood_flow_model", pattern: /^fluidFlow.*\.cas$/)
    create(:data_file_type,
           name: "Estimated parameters",
           viewer: :text,
           data_type: "estimated_parameters", pattern: /^0DModel_input\.csv$/)

    create(:approved_membership, user:, organization:)
    sign_in_as(user)

    patient.execute_data_sync(user)
  end

  it "shows data file diffs", js: true do
    visit comparisons_path(pipeline_ids: pipelines.map(&:iid))

    expect(page).to have_content "Estimated parameters"
    expect(page).to have_css "table.diff td.replace"
    expect(all("table.diff td.replace").map(&:text)).
      to match_array %w[0 WRONG! 0.965 96.5]
  end

  it "hides non-paired and noncomparable files", js: true do
    visit comparisons_path(pipeline_ids: pipelines.map(&:iid))

    expect(page).to have_content "Blood flow model. Not compared."
  end

  it "refuses to work for >2 pipelines" do
    visit comparisons_path(pipeline_ids: Pipeline.all.map(&:iid))

    expect(current_path).to eq patient_path(patient)
  end

  it "shows link to sources comparison when enough data is available" do
    visit comparisons_path(pipeline_ids: pipelines.map(&:iid))

    expect(page).not_to have_content "Sources comparison"

    pipelines[0].computations << create(:rimrock_computation)
    pipelines[1].computations << create(:rimrock_computation,
                                        tag_or_branch: "fixes",
                                        revision: "5678")

    visit comparisons_path(pipeline_ids: pipelines.map(&:iid))

    expect(page).to have_content "Sources comparison"
    step = pipelines[0].computations[0].step
    expect(page).to have_content "#{step.name}, rev. master:1234 vs rev. fixes:5678"
  end
end
=end
