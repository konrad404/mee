# frozen_string_literal: true

require "application_system_test_case"

class SideMenuTest < ApplicationSystemTestCase
  def setup
    in_organization! organizations("main")
    login_as users("user")
    visit root_path
  end

  test "link to pipelines index" do
    assert_text I18n.t("layouts.organization.side_menu.research.pipelines")
    click_link I18n.t("layouts.organization.side_menu.research.pipelines")

    assert_current_path pipelines_path
  end

  test "link to patients index" do
    assert_text I18n.t("layouts.organization.side_menu.research.patients")
    click_link I18n.t("layouts.organization.side_menu.research.patients")

    assert_current_path patients_path
  end

  test "link to artifacts" do
    assert_text I18n.t("layouts.organization.side_menu.research.artifacts")
    click_link I18n.t("layouts.organization.side_menu.research.artifacts")

    assert_current_path artifacts_path
  end

  test "link to cohorts" do
    assert_text I18n.t("layouts.organization.side_menu.research.cohorts")
    click_link I18n.t("layouts.organization.side_menu.research.cohorts")

    assert_current_path cohorts_path
  end

  test "link to flows" do
    assert_text I18n.t("layouts.organization.side_menu.pipelines_blueprints.flows")
    click_link I18n.t("layouts.organization.side_menu.pipelines_blueprints.flows")

    assert_current_path flows_path
  end

  test "link to steps" do
    assert_text I18n.t("layouts.organization.side_menu.pipelines_blueprints.steps")
    click_link I18n.t("layouts.organization.side_menu.pipelines_blueprints.steps")

    assert_current_path steps_path
  end

  test "link to help pipeline steps" do
    assert_text I18n.t("help.manual.step.title")
    click_link I18n.t("help.manual.step.title")

    assert_current_path help_page_path(category: :manual, file: :step)
  end

  test "storage usage progress bar" do
    Organization::StorageQuota.any_instance.stubs(:exceeded?).returns(true)
    visit root_path

    assert_text I18n.t("layouts.organization.side_menu.storage_usage.title").upcase
    assert_selector ".progress"
    assert_text I18n.t("layouts.organization.side_menu.storage_usage.quota_exceeded")
  end
end
