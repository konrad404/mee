# frozen_string_literal: true

require "test_helper"

class Patients::SummaryComponentTest < ViewComponent::TestCase
  include PunditHelper
  include ActionDispatch::TestProcess::FixtureFile

  setup do
    @patient = create(:patient)
  end

  test "gives the file number for each patient case" do
    @patient.inputs.attach(io: fixture_file_upload("outdated_proxy"), filename: "input 1")
    @patient.inputs.attach(io: fixture_file_upload("outdated_proxy"), filename: "input 2")

    render_inline(Patients::SummaryComponent.new(patient: @patient, pundit_user:))

    assert_text "#{I18n.t "patients.summary_component.files"}: 2"
  end

  test "gives the pipeline number for each patient case" do
    create(:pipeline, runnable: @patient)

    render_inline(Patients::SummaryComponent.new(patient: @patient, pundit_user:))

    assert_text "#{I18n.t "patients.summary_component.pipelines"}: 1"
  end

  test "shows last pipeline only when exists" do
    render_inline(Patients::SummaryComponent.new(patient: @patient, pundit_user:))
    assert_no_text I18n.t "patients.summary_component.last_pipeline"

    create(:pipeline, runnable: @patient)
    render_inline(Patients::SummaryComponent.new(patient: @patient, pundit_user:))
    assert_text I18n.t "patients.summary_component.last_pipeline"
  end
end
