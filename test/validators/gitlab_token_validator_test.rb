# frozen_string_literal: true

require "test_helper"

class Validatable
  include ActiveModel::Validations

  def initialize(gitlab_private_token)
    @host = "gitlab.com"
    @private_token = gitlab_private_token
    @repository = "cyfronet/mee-test"
  end

  attr_reader :private_token, :host, :repository
  validates :private_token, gitlab_token: {
    repository: ->(record) { record.git_config.repository }
  }
end

class GitlabTokenValidatorTest < ActiveSupport::TestCase
  include GitlabHelper

  setup do
    @model = Validatable.new("token")
  end

  test "reports nothing when private token is ok" do
    stub_gitlab_branches(repository: @model.repository,
                         status: 200, body: [{ name: "master" }])

    assert @model.valid?
  end

  test "reports token invalid" do
    stub_gitlab_branches(repository: @model.repository,
                        status: 401, body: { error: "wrong token" })

    assert_not @model.valid?
    assert_includes @model.errors[:private_token], "is invalid"
  end

  test "reports too low token privileges" do
    stub_gitlab_branches(repository: @model.repository,
                         status: 403,
                         body: {
                          error: "insufficient_scope",
                          error_description: "requires higher privileges",
                          scope: "api read_api"
                        })

    assert_not @model.valid?
    assert_includes @model.errors[:private_token],
                    "has to low privileges. At least 'read_api' role is needed"
  end
end
