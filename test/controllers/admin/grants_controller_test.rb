# frozen_string_literal: true

require "test_helper"

class Admin::GrantsControllerTest < ActionDispatch::IntegrationTest
  setup do
    in_organization! organizations("main")
    login_as users("admin")
  end

  test "I can remove grant" do
    assert_difference "Grant.count", -1 do
      delete admin_grant_path(grants("cpu"))
    end

    assert_redirected_to admin_grants_path
  end

  test "I cannot remove grant from other organization" do
    other_grant = create(:grant, organization: organizations("other"))

    assert_no_difference "Grant.count" do
      delete admin_grant_path(other_grant)
    end
  end
end
