# frozen_string_literal: true

require "test_helper"

class Admin::UsersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @organization = organizations("main")
    in_organization! @organization
  end

  test "admin can remove user from organization" do
    john = create(:user)
    membership = Membership.create!(user: john,
                                    organization: @organization,
                                    state: "approved")

    login_as(users("admin"))
    assert_changes "Membership.count", -1 do
      delete admin_user_path(membership)
    end
    assert_equal I18n.t("admin.users.destroy.success", user: john.name), flash[:notice]
  end

  test "admin cannot remove himself" do
    login_as(users("admin"))
    assert_no_changes "Membership.count" do
      delete admin_user_path(memberships("admin"))
    end
    assert_equal I18n.t("admin.users.destroy.self"), flash[:alert]
  end

  test "user cannot enter users management page" do
    login_as(users("user"))
    get admin_users_path

    assert_response :redirect
  end

  test "user cannot change users state" do
    login_as(users("user"))
    put admin_user_path(memberships("user"), state: :blocked)

    assert_response :redirect
  end
end
