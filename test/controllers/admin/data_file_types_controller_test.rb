# frozen_string_literal: true

require "test_helper"

class Admin::DataFileTypesControllerTest < ActionDispatch::IntegrationTest
  setup do
    in_organization! organizations("main")
    login_as users("admin")
  end

  test "should destroy data file type from owned organization" do
    assert_difference "DataFileType.count", -1 do
      delete admin_data_file_type_url(data_file_types("image"))
    end

    assert_redirected_to admin_data_file_types_url
  end

  test "cannot remove data file type from other organization" do
    other_dft = create(:data_file_type, organization: organizations("other"))

    assert_no_difference "DataFileType.count" do
      delete admin_data_file_type_url(other_dft)
    end

    assert_redirected_to root_url
  end
end
