# frozen_string_literal: true

require "test_helper"

class Admin::OrganizationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @organization = organizations("main")
    in_organization! @organization
  end

  test "cannot be deleted by normal user" do
    login_as users("user")

    assert_no_difference "Organization.count" do
      delete admin_organization_path(@organization)
    end

    assert_redirected_to root_path
  end

  test "can be deleted by organization admin" do
    login_as users("admin")

    assert_difference "Organization.count", -1 do
      delete admin_organization_path(@organization)
    end
  end
end
