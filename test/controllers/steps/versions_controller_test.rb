# frozen_string_literal: true

require "test_helper"

class Steps::VersionsControllerTest < ActionDispatch::IntegrationTest
  include GitlabHelper

  setup do
    @step = steps("first")
    @memory_store = ActiveSupport::Cache
      .lookup_store(:memory_store, expires_in: 10.minutes)

    Rails.stubs(:cache).returns(@memory_store)
    Rails.cache.clear

    in_organization! organizations("main")
    login_as users("user")
  end

  test "fetch versions from cache" do
    stub_repo_versions(@step,
                       { branches: ["A", "B"], tags: ["T1", "T2"] },
                       { branches: [], tags: [] })

    get step_versions_path(@step)
    # from cache
    get step_versions_path(@step)

    assert_equal 200, response.status
    ["A", "B", "T1", "T2"].each do |v|
      assert_match v, response.body, "Should have #{v} option"
    end
  end

  test "skip cache when force reload" do
    stub_repo_versions(@step,
                       { branches: ["A"], tags: ["T1"] },
                       { branches: ["A", "B"], tags: ["T1", "T2"] })

    get step_versions_path(@step)
    # new Gitlab API request
    get step_versions_path(@step, force_reload: true)

    assert_equal 200, response.status
    ["A", "B", "T1", "T2"].each do |v|
      assert_match v, response.body, "Should have #{v} option"
    end
  end
end
