# frozen_string_literal: true

module DataverseTestHelper
  def dataverse_test_setup
    computation = build(:computation)

    organization = organizations("main")
    membership = memberships("user")

    [ computation, organization.dataverse_url, membership.dataverse_token ]
  end
end
