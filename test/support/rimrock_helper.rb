# frozen_string_literal: true


module RimrockHelper
  def stub_rimrock_process(with_error: false)
    if with_error
      stub_request(:post, "https://rimrock.plgrid.pl/api/process")
        .to_return(status: 400, body: "")
    else
      stub_request(:post, "https://rimrock.plgrid.pl/api/process")
        .to_return(status: 200, body: "{\"status\": \"OK\", \"exit_code\": 0, \"standard_output\": \"\"}")
    end
  end

  def stub_job_abort(computation, status: 200)
    stub_request(:put, "#{rimrock_url}/api/jobs/#{computation.job_id}")
      .with(body: { action: "abort" }.to_json)
      .to_return(status:)
  end

  def stub_computation_logs_request(*computations)
    computations.each do |computation|
      stub_request(:get, "https://data.plgrid.pl/download/#{computation.site.host_key}/#{computation.stdout_path}")
        .to_return(status: 200, body: "stdout", headers: {})
      stub_request(:get, "https://data.plgrid.pl/download/#{computation.site.host_key}/#{computation.stderr_path}")
        .to_return(status: 200, body: "stderr", headers: {})
    end
  end

  def stub_plgrid_api_jobs_get(computations, with_error: false)
    status = with_error ? "ERROR" : "FINISHED"
    body = computations.map { |c| { job_id: c.job_id, status:, stdout_path: "some_path", stderr_path: "some_stderr" } }.to_json
    job_ids = computations.map(&:job_id).join("&job_id=")
    stub_request(:get, "https://rimrock.plgrid.pl/api/jobs?format=short&job_id=#{job_ids}")
      .to_return(status: 200, body:)
  end

  private
    def rimrock_url
      Rails.application.config.constants.dig(:rimrock, :url)
    end
end
