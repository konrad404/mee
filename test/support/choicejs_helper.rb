# frozen_string_literal: true

module ChoiceJSHelper
  def choicejs_select(value, from:)
    parent_div = find("label", text: from).find(:xpath, "..")
    parent_div.find("div.choices__inner").click
    parent_div.find("div.choices__item", text: value).click
  end
end
